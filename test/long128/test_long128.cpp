#include <stdio.h>
#include "unity.h"
#include <Long128.h>

#define DEBUG_INFO

void printVariable(Long128 a) {
    #ifdef DEBUG_INFO
    printf("0x%016llx %016llx\n", a.high, a.low);
    #endif
}

/**
 * Dieser Test überprüft, ob der Überlauf zwischen high- und low-Speicher bei der Addition funktioniert.
 */
void test_addition() {
    Long128 a = Long128(0, uint64_t(0x8000000000000000)) + 30;
    Long128 b = a+1;
    Long128 r(1, 61);

    #ifdef DEBUG_INFO
    printf("0x%016llx %016llx + 0x%016llx %016llx = 0x%016llx %016llx\n", a.high, a.low, b.high, b.low, r.high, r.low);
    #endif
    TEST_ASSERT_MESSAGE(a+b == r, "a+b Failed!");
}
/**
 * Dieser Test überprüft, ob der Überlauf zwischen high- und low-Speicher bei der Subtraktion funktioniert.
 */
void test_subtraction() {
    Long128 a(1, 0);
    Long128 b(0, 1);
    Long128 r(0, 0xFFFFFFFFFFFFFFFF);
    #ifdef DEBUG_INFO
    printf("0x%016llx %016llx - 0x%016llx %016llx = 0x%016llx %016llx\n", a.high, a.low, b.high, b.low, r.high, r.low);
    #endif
    TEST_ASSERT_MESSAGE(a-b == r, "a-b Failed!");
}

void test_div() {
    Long128 a(2, 9);
    Long128 b = 3;

    Long128 r(0, 0xaaaaaaaaaaaaaaad);
    #ifdef DEBUG_INFO
    Long128 printMe = ((Long128)-0xAF67D0)/16;
    printf("0x%016llx %016llx\n", printMe.high, printMe.low);
    #endif
    TEST_ASSERT_MESSAGE(a/b == r, "a/b Failed!");
    TEST_ASSERT_MESSAGE(-a/b == -r, "-a/b Failed!");
    TEST_ASSERT_MESSAGE(a/-b == -r, "a/-b Failed!");
    TEST_ASSERT_MESSAGE(-a/-b == r, "-a/-b Failed!");
    TEST_ASSERT_MESSAGE(a/0 == LONG128INF, "a/0 Failed!");
    TEST_ASSERT_MESSAGE(-a/0 == LONG128NINF, "-a/0 Failed!");
    TEST_ASSERT_MESSAGE((Long128)0/0 == LONG128NAN, "0/0 Failed!");
    TEST_ASSERT_MESSAGE((Long128)0xAF67D0/16 == Long128(0xAF67D), "a/16 Failed!");
    TEST_ASSERT_MESSAGE((Long128)-0xAF67D0/16 == -Long128(0xAF67D), "-a/16 Failed!");
    TEST_ASSERT_MESSAGE((Long128)0xAF67D0/-16 == -Long128(0xAF67D), "a/-16 Failed!");
    TEST_ASSERT_MESSAGE(a/LONG128INF == 0, "a/LONG128INF Failed!");
    TEST_ASSERT_MESSAGE(a/LONG128NINF == 0, "a/LONG128NINF Failed!");
    TEST_ASSERT_MESSAGE(a/(a+2) == 0, "a/(>a) Failed!");
}

void test_exactMul() {
    int64_t a = 0xABCDEF012345567;
    int64_t b = 25;

    Long128 r(0x1, 0xc71c571c71c570f);

    Long128 realResult = exactMul(a, b);
    #ifdef DEBUG_INFO
    printf("0x%016llx %016llx\n", realResult.high, realResult.low);
    #endif
    TEST_ASSERT_MESSAGE(exactMul(a, b) == r, "exactMul(a, b) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(-a, b) == -r, "exactMul(-a, b) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(a, -b) == -r, "exactMul(a, -b) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(-a, -b) == r, "exactMul(-a, -b) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(a, 0) == 0, "exactMul(a, 0) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(0, b) == 0, "exactMul(0, b) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(a, 16) == (Long128)a<<4, "exactMul(a, 16) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(-a, 16) == -(Long128)a<<4, "exactMul(-a, 16) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(a, -16) == -(Long128)a<<4, "exactMul(a, -16) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(-a, -16) == (Long128)a<<4, "exactMul(-a, -16) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(32, a) == (Long128)a<<5, "exactMul(32, a) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(-32, a) == -(Long128)a<<5, "exactMul(-32, a) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(32, -a) == -(Long128)a<<5, "exactMul(32, -a) Failed!");
    TEST_ASSERT_MESSAGE(exactMul(-32, -a) == (Long128)a<<5, "exactMul(-32, -a) Failed!");
}

void test_greater_less() {
    Long128 a = 50;
    TEST_ASSERT_MESSAGE(a > 0, "a>0 Failed!");
    TEST_ASSERT_MESSAGE((a < 0) == 0, "a<0 Failed!");
}

void test_multiplication() {
    Long128 a(0b1010, 0b101010);
    Long128 b = 56;

    Long128 r = Long128(0x230, 0x930);
    #ifdef DEBUG_INFO
    printf("0x%016llx %016llx * 0x%016llx %016llx = 0x%016llx %016llx\n", a.high, a.low, b.high, b.low, r.high, r.low);
    #endif
    TEST_ASSERT_MESSAGE(a * b == r, "a*b Failed!");
    TEST_ASSERT_MESSAGE(-a * b == -r, "-a*b Failed!");
    TEST_ASSERT_MESSAGE(a * -b == -r, "a*-b Failed!");
    TEST_ASSERT_MESSAGE(-a * -b == r, "-a*-b Failed!");
    TEST_ASSERT_MESSAGE(a * 0 == 0, "a*0 Failed!");
    TEST_ASSERT_MESSAGE((Long128)0 * b == 0, "0*b Failed!");
    TEST_ASSERT_MESSAGE(a * 64 == a<<6, "a*64 Failed!");
    TEST_ASSERT_MESSAGE(-a * 64 == -a<<6, "-a*64 Failed!");
    TEST_ASSERT_MESSAGE(a * -64 == -a<<6, "a*-64 Failed!");
    TEST_ASSERT_MESSAGE(-a * -64 == a<<6, "-a*-64 Failed!");
    TEST_ASSERT_MESSAGE((Long128)128 * b == b<<7, "128*b Failed!");
    TEST_ASSERT_MESSAGE((Long128)-128 * b == -b<<7, "-128*b Failed!");
    TEST_ASSERT_MESSAGE((Long128)128 * -b == -b<<7, "128*-b Failed!");
    TEST_ASSERT_MESSAGE((Long128)-128 * -b == b<<7, "-128*-b Failed!");
}

void test_leftshift() {
    Long128 a(0b101, (uint64_t)1<<63 | 0xAF);
    Long128 r = a << 1;

    TEST_ASSERT_MESSAGE(r == Long128(0b1011, 0b101011110), "a << 1 Failed!");

    r = a << 65;
    TEST_ASSERT_MESSAGE(r == Long128(0b101011110, 0), "a << 65 Failed!");
}

void test_rightshift() {
    Long128 a(0b101, (uint64_t)1<<63 | 0xAF);
    Long128 r = a >> 1;

    TEST_ASSERT_MESSAGE(r == Long128(0b10, (uint64_t)0b11<<62 | 0b1010111), "a >> 1 Failed!");

    r = a >> 65;
    TEST_ASSERT_MESSAGE(r == Long128(0, 0b10), "a << 65 Failed!");
}

void test_or() {
    Long128 a(0xABCDEF, 0x1234567890);
    Long128 r = a | 0xFF00FF00FF;

    printVariable(r);
    TEST_ASSERT_MESSAGE(r == Long128(0xABCDEF, 0xFF34FF78FF), "a | int64_t Failed!");

    r = a | Long128(0xFF0, 0xFF00FF00FF);

    TEST_ASSERT_MESSAGE(r == Long128(0xABCFFF, 0xFF34FF78FF), "a | Long128 Failed!");
}

void test_and() {
    Long128 a(0xABCDEF, 0x1234567890);
    Long128 r = a & 0xFF00FF00FF;

    printVariable(r);
    TEST_ASSERT_MESSAGE(r == Long128(0, 0x1200560090), "a & int64_t Failed!");

    r = a & Long128(0xFF0, 0xFF00FF00FF);

    TEST_ASSERT_MESSAGE(r == Long128(0xDE0, 0x1200560090), "a & Long128 Failed!");
}

void test_xor() {
    Long128 a(0xABCDEF, 0x1234567890);
    Long128 r = a ^ 0xFF00FF00FF; 

    printVariable(r);
    TEST_ASSERT_MESSAGE(r == Long128(0, 0xED34A9786F), "a ^ int64_t Failed!");

    r = a & Long128(0xFF0, 0xFF00FF00FF);

    TEST_ASSERT_MESSAGE(r == Long128(0xABC21F, 0xED34A9786F), "a ^ Long128 Failed!");
}

int main() {
    UNITY_BEGIN();

    // Mathematische Operatoren
    RUN_TEST(test_addition);
    RUN_TEST(test_subtraction);
    RUN_TEST(test_div);
    RUN_TEST(test_exactMul);
    RUN_TEST(test_multiplication);

    // Binäre Operationen
    RUN_TEST(test_leftshift);
    RUN_TEST(test_rightshift);

    // Boolesche Operationen
    RUN_TEST(test_greater_less);
    RUN_TEST(test_or);
    RUN_TEST(test_and);

    UNITY_END();

    return 0;
}
