#include <stdio.h>
#include "unity.h"
#include <fp32.h>

using fixpoint_t = fixpoint32_t;

void TEST_DOUBLE(double expected, double actual, const char* message) {
    double within = 0.01;
    TEST_ASSERT_MESSAGE((((expected + within) > actual) && ((expected - within) < actual)), message);
}

void test_toDouble() {
    fixpoint_t a(50.678);
    TEST_DOUBLE(50.678, a.toDouble(), "toDouble Failed!");
}

void test_invert() {
    fixpoint_t a(5);
    printf("%f\n", a.invert().toDouble());
    TEST_DOUBLE(0.2, a.invert().toDouble(), "invert Failed!");
}

void test_pow() {
    fixpoint_t a(3);

    printf("%f\n", a.pow(3).toDouble());
    TEST_DOUBLE(27, a.pow(3).toDouble(), "pow Failed!");
}

void test_mul() {
    fixpoint_t a(3.62);
    printf("%f\n", (a * 10.0).toDouble());
    TEST_DOUBLE(36.2, (a * fixpoint_t(10.0)).toDouble(), "mul Failed!");
}

void test_div() {
    fixpoint_t a(25.66);
    printf("%f\n", (a / 2).toDouble());
    TEST_DOUBLE(12.83, (a / 2).toDouble(), "div Failed!");
}

void test_add() {
    fixpoint_t a(25.66);

    printf("%f\n", (a + 2.345).toDouble());
    TEST_DOUBLE(28.005, (a + 2.345).toDouble(), "add Failed!");
}

void test_sub() {
    fixpoint_t a(25.66);

    printf("%f\n", (a - 2.345).toDouble());
    TEST_DOUBLE(23.315, (a - 2.345).toDouble(), "sub Failed!");
}

void test_equals() {
    fixpoint_t a(12.34);
    fixpoint_t b(12.34);

    TEST_ASSERT_MESSAGE(a == b, "== Failed!");
}

void test_less() {
    fixpoint_t a(5.678);
    fixpoint_t b(12.34);

    TEST_ASSERT_MESSAGE(a < b, "< Failed!");
}

void test_less_equals() {
    fixpoint_t a(5.678);
    fixpoint_t b(12.34);
    fixpoint_t c(5.678);

    TEST_ASSERT_MESSAGE((a <= b && a <= c), "<= Failed!");
}

void test_greater() {
    fixpoint_t a(5.678);
    fixpoint_t b(12.34);

    TEST_ASSERT_MESSAGE(b > a, "> Failed!");
}

void test_greater_equals() {
    fixpoint_t a(5.678);
    fixpoint_t b(12.34);
    fixpoint_t c(5.678);

    TEST_ASSERT_MESSAGE((b >= a && a >= c), ">= Failed!");
}

void test_sqrt() {
    fixpoint_t a(2);

    printf("%f\n", (a.sqrt()).toDouble());
    TEST_DOUBLE(1.4142, a.sqrt().toDouble(), "sqrt Failed!");
}

int main() {
    UNITY_BEGIN();

    RUN_TEST(test_toDouble);
    RUN_TEST(test_invert);
    RUN_TEST(test_pow);
    RUN_TEST(test_mul);
    RUN_TEST(test_div);
    RUN_TEST(test_add);
    RUN_TEST(test_sub);
    RUN_TEST(test_equals);
    RUN_TEST(test_less);
    RUN_TEST(test_less_equals);
    RUN_TEST(test_greater);
    RUN_TEST(test_greater_equals);
    RUN_TEST(test_sqrt);

    UNITY_END();

    return 0;
}