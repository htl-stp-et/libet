#include "unity.h"
#include <iostream>

#include <Winkel.h>

void TEST_DOUBLE(double expected, double actual, const char* message) {
    double within = 0.001;
    TEST_ASSERT_MESSAGE((((expected + within) > actual) && ((expected - within) < actual)), message);
}

void printFP(fixpoint32_t fp) {
    std::cout << fp.toDouble() << "\n";
}

void test_sin() {
    Winkel w = 30;
    printFP(w.sin());
    TEST_DOUBLE(0.5, w.sin().toDouble(), "sin Failed!");
}

void test_cos() {
    Winkel w = 60;
    printFP(w.cos());
    TEST_DOUBLE(0.5, w.cos().toDouble(), "cos Failed!");
}

void test_tan() {
    Winkel w = 30;
    printFP(w.tan());
    TEST_DOUBLE(0.57735, w.tan().toDouble(), "tan Failed!");
}

void test_atan() {
    fixpoint32_t fp1 = 3;
    fixpoint32_t fp2 = 2;

    Winkel w = Winkel::atan2(fp1, fp2);
    printFP(w.toRad());
    TEST_DOUBLE(0.982, w.toRad().toDouble(), "atan Failed!");

}

int main() {
    UNITY_BEGIN();

    RUN_TEST(test_sin);
    RUN_TEST(test_cos);
    RUN_TEST(test_tan);
    // RUN_TEST(test_atan);

    UNITY_END();
    return 0;
}