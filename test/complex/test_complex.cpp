#include <iostream>
#include "unity.h"
#include <Complex.h>

void printcomplex(complex32_t a) {
    std::cout << a.real_.toDouble() << " + j" << a.imag_.toDouble() << "\n";
}

bool tdouble(double expected, double actual) {
    double within = 0.01;
    return ((expected + within) > actual) && ((expected - within) < actual);
}

void TEST_DOUBLE(double expected, double actual, const char* message) {
    
    TEST_ASSERT_MESSAGE(tdouble(expected, actual), message);
}

void TEST_COMPLEX(complex32_t expected, complex32_t actual, const char* message) {
    TEST_ASSERT_MESSAGE(tdouble(expected.real_.toDouble(), actual.real_.toDouble()) && tdouble(expected.imag_.toDouble(), actual.imag_.toDouble()), message);
}

void test_abs() {
    complex32_t a(5, 5);
    TEST_DOUBLE(7.071, a.abs().toDouble(), "Abs Failed!");
}

void test_add() {
    complex32_t a(3, 4);
    complex32_t b(0.5, 6.7);
    TEST_COMPLEX(complex32_t(3.5, 10.7), a+b, "Addition Failed!");
}

void test_sub() {
    complex32_t a(3, 4);
    complex32_t b(0.5, 6.7);
    TEST_COMPLEX(complex32_t(2.5, -2.7), a-b, "Subtraction Failed!");
}

void test_mul() {
    complex32_t a(0.5253, 0.8509); // 1 < 45°
    fixpoint32_t skalar = 5.5;
    printcomplex(a*skalar);
    TEST_COMPLEX(complex32_t(2.8891, 4.679), a*skalar, "Scalar multiplication Failed!");
    printcomplex(a*a);
    TEST_COMPLEX(complex32_t(-0.44809, 0.8939), a*a, "Complex multiplication Failed!");
}

void test_div() {
    complex32_t a(15, 13.4);
    complex32_t b(4.6, 5.7);
    fixpoint32_t scalar = 3.4;
    TEST_COMPLEX(complex32_t(4.4117, 3.9411), a/scalar, "Scalar division Failed!");
    TEST_COMPLEX(complex32_t(2.709, -0.4447), a/b, "Complex division Failed!");
}

void test_inv() {
    complex32_t a(15, 13.4);
    printcomplex(a.inv());
    TEST_COMPLEX(complex32_t(0.037, -0.0331), a.inv(), "Invert Failed!");
}

void test_conj() {
    complex32_t a(15, 13.4);
    printcomplex(a.conj());
    TEST_COMPLEX(complex32_t(15, -13.4), a.conj(), "Conj Failed!");
}

void test_normalize() {
    complex32_t a(2.3, 4.5);
    printcomplex(a.abs());
    printcomplex(a.normalize());
    TEST_COMPLEX(complex32_t(0.4551, 0.8904), a.normalize(), "Conj Failed!");
}

int main() {
    UNITY_BEGIN();
    
    RUN_TEST(test_abs);
    RUN_TEST(test_add);
    RUN_TEST(test_sub);
    RUN_TEST(test_mul);
    RUN_TEST(test_div);
    RUN_TEST(test_inv);
    RUN_TEST(test_conj);
    RUN_TEST(test_normalize);

    UNITY_END();

    return 0;
}