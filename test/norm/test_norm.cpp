#include <stdio.h>
#include <unity.h>
#include <Norm.h>

using norm_int16_t = Norm<int16_t>;

void TEST_DOUBLE(double expected, double actual, const char* message) {
    double within = 0.01;
    bool check = (((expected + within) > actual) && ((expected - within) < actual));
    printf("%f ", actual);
    TEST_ASSERT_MESSAGE(check, message);
}

void test_init() {
    norm_int16_t a(0, 1024, 512);
    TEST_DOUBLE(-1.0, a.getNormated().toDouble(), "Init Failed!");
}

void test_update() {
    norm_int16_t a(0, 1024, 512);
    a.update(677);

    TEST_DOUBLE(0.322265625, a.getNormated().toDouble(), "Update Failed!");
}

void test_denorm() {
    norm_int16_t a(666, 1024, 512);
    TEST_DOUBLE(666, a.denorm(), "Denorm Failed!");

    norm_int16_t b(666, 1024, 512);
    TEST_DOUBLE(166, a.denorm(256, 128), "Denorm Failed!");
}

int main() {
    UNITY_BEGIN();

    RUN_TEST(test_init);
    RUN_TEST(test_update);
    RUN_TEST(test_denorm);

    UNITY_END();
    return 0;
}