#include <EasyADC.hpp>

static void (*funcPtr)(uint16_t adcValue);

#ifdef __AVR_ATmega2560__

EasyADC::EasyADC(int8_t adcNumber, uint8_t prescaler){
    this->adcPort = adcNumber;

    if (adcNumber <= 7){
        DDRF &= ~(1 << adcNumber);
        PORTF &= ~(1 << adcNumber);
    } else {
        DDRK &= ~(1 << (7 & adcNumber));
        PORTK &= ~(1 << (7 & adcNumber));
    }

    ADCSRA = (1 << ADEN) | (prescaler << ADPS0);    
    ADCSRB = 0;
}

uint16_t EasyADC::adcReadPoll(){
    ADMUX = (1 << REFS0) | ((7 & this->adcPort) << MUX0);

    if (this->adcPort > 7)
        ADCSRB = (1 << MUX5);

    // disabling ADC Interrupt
    ADCSRA &= ~(1 << ADIE);

    //conversion start
    ADCSRA |= (1 << ADSC);

    //pollling auf conversion status
    while(!(ADCSRA & (1 << ADIF)));

    return ADC;
}

void EasyADC::adcReadInt(void runFunction(uint16_t returnedValue)){

    funcPtr = runFunction;

    //Auswahl des entsprechenden ADCs
    ADMUX = (1 << REFS0) | ((7 & this->adcPort) << MUX0);
    if (this->adcPort > 7)
        ADCSRB = (1 << MUX5);

    //enabling ADC Interrupt
    ADCSRA |= (1 << ADIE);

    //start conversion
    ADCSRA |= (1 << ADSC);
}

ISR(ADC_vect){
    ADCSRA &= ~(1 << ADIE);
    funcPtr(ADC);
}

#endif

#if defined (__arm__) && defined (__SAM3X8E__)
#include <chip.h>

static EasyADC* adcInt;

EasyADC::EasyADC(int8_t adcNumber, uint8_t prescaler){
    this->adcPort = adcNumber;

    ADC->ADC_CR = ADC_CR_SWRST;
    ADC->ADC_MR = (ADC_MR_PRESCAL(255));
    NVIC_EnableIRQ(ADC_IRQn);
}

uint16_t EasyADC::adcReadPoll(){
    ADC->ADC_CHER = (1 << this->adcPort);
    ADC->ADC_CR = (1 << ADC_CR_START);

    while(!(ADC->ADC_ISR & (1 << this->adcPort)));
    return ADC->ADC_CDR[this->adcPort];
}

void EasyADC::adcReadInt(void runFunction(uint16_t returnedValue)){

    REG_ADC_CHER = (1 << this->adcPort);

    REG_ADC_IER = (1 << this->adcPort);
    REG_ADC_IDR &= ~(1 << this->adcPort);

    funcPtr = runFunction;
    adcInt = this;
    REG_ADC_CR = (1 << ADC_CR_START);
}

void ADC_Handler(){
    REG_ADC_IER &= ~(1 << adcInt->adcPort);
    REG_ADC_IDR = (1 << adcInt->adcPort);    
    funcPtr(ADC->ADC_CDR[adcInt->adcPort]);
}

#endif