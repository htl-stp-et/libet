#include <Arduino.h>

class EasyADC{
    public:
        EasyADC(int8_t adcNumber, uint8_t prescaler);
        uint16_t adcReadPoll();
        void adcReadInt(void runFunction(uint16_t returnedValue));
    private:
        int8_t adcPort;

        friend void ADC_Handler();
};