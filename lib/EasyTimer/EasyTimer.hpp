#pragma once

#include<Arduino.h>

class EasyTimer{
    public:
        EasyTimer(uint8_t timer);
        void start();
        void stop();
        void setCallback(void (*callback)());
    private:
        uint8_t timerNumber;
        uint16_t addrTCCR[2];
        uint16_t addrOCR[3];
        uint16_t addrTIMSK;

};
