#include<EasyTimer.hpp>


#ifdef __AVR_ATmega2560__

EasyTimer::EasyTimer(uint8_t timer){
    timerNumber = timer;

    switch(timer){
    case 0:    
        addrTCCR[0] = 0x24;
        addrTCCR[1] = 0x25;
        addrOCR[0] = 0x27;
        addrOCR[1] = 0x28;
        addrTIMSK = 0x6E;
        break;
    case 1:
        addrTCCR[0] = 0x80;
        addrTCCR[1] = 0x81;
        addrOCR[0] = 0x88;
        addrOCR[1] = 0x8A;
        addrOCR[2] = 0x8C;
        addrTIMSK = 0x6F;
        break;         
    case 2:    
        addrTCCR[0] = 0xB0;
        addrTCCR[1] = 0xB1;
        addrOCR[0] = 0xB3;
        addrOCR[1] = 0xB4;
        addrTIMSK = 0x70;
        break;       
    case 3:
        addrTCCR[0] = 0x90;
        addrTCCR[1] = 0x91;
        addrOCR[0] = 0x98;
        addrOCR[1] = 0x9A;
        addrOCR[2] = 0x9C;
        addrTIMSK = 0x71;
        break;    
    case 4:
        addrTCCR[0] = 0xA0;
        addrTCCR[1] = 0xA1;
        addrOCR[0] = 0xA8;
        addrOCR[1] = 0xAA;
        addrOCR[2] = 0xAC;
        addrTIMSK = 0x72;
        break;   
    case 5:
        addrTCCR[0] = 0x120;
        addrTCCR[1] = 0x121;
        addrOCR[0] = 0x128;
        addrOCR[1] = 0x12A;
        addrOCR[2] = 0x12C;
        addrTIMSK = 0x73;
        break;     
    }

    sei();
}

void EasyTimer::start(){
    switch(timerNumber){
    case 0:
        _SFR_IO8(addrTCCR[0]) = (2 << WGM00);
        _SFR_IO8(addrTCCR[1]) = (3 << CS00);
        _SFR_IO8(addrOCR[0]) = 124;

        break;
    case 2:
        _SFR_MEM8(addrTCCR[0]) = (2 << WGM20);
        _SFR_MEM8(addrTCCR[1]) = (4 << CS20);
        _SFR_MEM8(addrOCR[0]) = 124;
        break;
    default:
        _SFR_MEM8(addrTCCR[0]) = 0;
        _SFR_MEM8(addrTCCR[1]) = (1 << 3) | (3 << 0);
        _SFR_MEM16(addrOCR[0]) = 124;
        break;
    }

    _SFR_MEM8(addrTIMSK) = (1 << 1);
}

void EasyTimer::stop(){
    _SFR_MEM8(addrTIMSK) &= ~(1 << 1);
}
#endif


#if defined (__arm__) && defined (__SAM3X8E__)


#endif