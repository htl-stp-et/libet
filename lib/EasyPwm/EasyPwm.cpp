#include <EasyPwm.hpp>



#if defined (__arm__) && defined (__SAM3X8E__)
#include<chip.h>

EasyPwm::EasyPwm(uint8_t timer, uint8_t outputType, uint32_t frequency){  
    uint32_t periodValue;

    currentOutputType = outputType;

    switch(outputType){
    case 0:
        periodValue = VARIANT_MCK/frequency;
        currentFrequency = periodValue;
        break;
    case 1:
        periodValue = VARIANT_MCK/(frequency * 2);
        currentFrequency = periodValue;
        break;
    }


    timerNum = timer;

    PMC->PMC_PCER1 |= PMC_PCER1_PID36;
    
    switch(timer){
    case 0:
        PIOC->PIO_ABSR |= PIO_ABSR_P2 | (PIO_ABSR_P3);
        PIOC->PIO_PDR |= PIO_PER_P2 | PIO_PER_P3;
        break;
    case 1:
        PIOC->PIO_ABSR |= PIO_ABSR_P4 | (PIO_ABSR_P5);
        PIOC->PIO_PDR |= PIO_PER_P4 | PIO_PER_P5;
        break;
    case 2:
        PIOA->PIO_ABSR |= PIO_ABSR_P20;
        PIOC->PIO_ABSR |= PIO_ABSR_P7;
        PIOA->PIO_PDR |= PIO_PER_P20;
        PIOC->PIO_PDR |= PIO_PER_P7;
        break;
    case 3:
        PIOC->PIO_ABSR |= PIO_ABSR_P8 | (PIO_ABSR_P9);
        PIOC->PIO_PDR |= PIO_PER_P8 | PIO_PER_P9;
        break;
    case 4:
        PIOC->PIO_ABSR |= PIO_ABSR_P21;
        PIOC->PIO_PDR |= PIO_PER_P21;
        break;
    case 5:
        PIOC->PIO_ABSR |= PIO_ABSR_P22 | (PIO_ABSR_P19);
        PIOC->PIO_PDR |= PIO_PER_P22 | PIO_PER_P19;
        break;
    case 6:
        PIOC->PIO_ABSR |= PIO_ABSR_P23 | (PIO_ABSR_P18);
        PIOC->PIO_PDR |= PIO_PER_P23 | PIO_PER_P18;
        break;
    case 7:
        PIOC->PIO_ABSR |= PIO_ABSR_P24;
        PIOC->PIO_PDR |= PIO_PER_P24;
        break;
    }

    PWM->PWM_CLK = PWM_CLK_DIVA(1) | PWM_CLK_DIVB(0) | PWM_CLK_PREA(0) | PWM_CLK_PREB(0);

    PWM->PWM_CH_NUM[timer].PWM_CMR = PWM_CMR_CPRE_CLKA | ((outputType&1) << 8);

    PWM->PWM_CH_NUM[timer].PWM_CPRD = PWM_CPRD_CPRD(periodValue); 

    PWM->PWM_CH_NUM[timer].PWM_CDTY = (PWM->PWM_CH_NUM[timer].PWM_CPRD / 2);
}

void EasyPwm::setSync(bool state){
    if(state)
        PWM->PWM_SCM |= (1 << timerNum) | PWM_SCM_UPDM_MODE0;
    else
        PWM->PWM_SCM &= ~(1 << timerNum) | PWM_SCM_UPDM_MODE0;
}

void EasyPwm::startSynced(){
    PWM->PWM_SCUC = PWM_SCUC_UPDULOCK;
}

void EasyPwm::pwmOnOff(bool state){
    if(state)
        PWM->PWM_ENA |= (1 << timerNum);
    else
        PWM->PWM_ENA &= ~(1 << timerNum);

}

void EasyPwm::setDutyCyle(uint64_t percentage){
    PWM->PWM_CH_NUM[timerNum].PWM_CDTYUPD = PWM_CDTYUPD_CDTYUPD((percentage*currentFrequency)/100);
}

void EasyPwm::setFrequency(uint32_t frequ){
    switch(currentOutputType){
    case 0:
        PWM->PWM_CH_NUM[timerNum].PWM_CPRDUPD = PWM_CPRDUPD_CPRDUPD(VARIANT_MCK/frequ);
        break;
    case 1:
        PWM->PWM_CH_NUM[timerNum].PWM_CPRDUPD = PWM_CPRDUPD_CPRDUPD(VARIANT_MCK/(2*frequ));
        break;
    }
}

#endif




#ifdef __AVR_ATmega2560__


EasyPwm::EasyPwm(uint8_t timer, uint8_t outputType, uint8_t prescalSet){
    timerNum = timer;    
    prescal = prescalSet;
    output = outputType;

    switch(timer){
    case 0:    
        addrTCC[0] = 0x24;
        addrTCC[1] = 0x25;
        addrOut[0] = 0x27;
        addrOut[1] = 0x28;

        addrDataDir[0] = 0x04;
        addrDataDir[1] = 0x13;
        addrDataPin[0] = 7;
        addrDataPin[1] = 5;
        break;
    case 1:
        addrTCC[0] = 0x80;
        addrTCC[1] = 0x81;
        addrOut[0] = 0x88;
        addrOut[1] = 0x8A;
        addrOut[2] = 0x8C;

        addrDataDir[0] = 0x04;
        addrDataDir[1] = 0x04;
        addrDataDir[2] = 0x04;
        addrDataPin[0] = 5;
        addrDataPin[1] = 6;
        addrDataPin[2] = 7;
        break;         
    case 2:    
        addrTCC[0] = 0xB0;
        addrTCC[1] = 0xB1;
        addrOut[0] = 0xB3;
        addrOut[1] = 0xB4;

        addrDataDir[0] = 0x04;
        addrDataDir[1] = 0x101;
        addrDataPin[0] = 4;
        addrDataPin[1] = 6;
        break;       
    case 3:
        addrTCC[0] = 0x90;
        addrTCC[1] = 0x91;
        addrOut[0] = 0x98;
        addrOut[1] = 0x9A;
        addrOut[2] = 0x9C;

        addrDataDir[0] = 0x0D;
        addrDataDir[1] = 0x0D;
        addrDataDir[2] = 0x0D;
        addrDataPin[0] = 3;
        addrDataPin[1] = 4;
        addrDataPin[2] = 5;
        break;    
    case 4:
        addrTCC[0] = 0xA0;
        addrTCC[1] = 0xA1;
        addrOut[0] = 0xA8;
        addrOut[1] = 0xAA;
        addrOut[2] = 0xAC;

        addrDataDir[0] = 0x101;
        addrDataDir[1] = 0x101;
        addrDataDir[2] = 0x101;
        addrDataPin[0] = 3;
        addrDataPin[1] = 4;
        addrDataPin[2] = 5;  
        break;   
    case 5:
        addrTCC[0] = 0x120;
        addrTCC[1] = 0x121;
        addrOut[0] = 0x128;
        addrOut[1] = 0x12A;
        addrOut[2] = 0x12C;

        addrDataDir[0] = 0x10A;
        addrDataDir[1] = 0x10A;
        addrDataDir[2] = 0x10A;
        addrDataPin[0] = 3;
        addrDataPin[1] = 4;
        addrDataPin[2] = 5;
        break;     
    }

    sei();
}

void EasyPwm::init(){
    if(isInitialized == false){
        isInitialized = true;
        switch(timerNum){
        case 0:
            _SFR_IO8(addrTCC[0]) = ((1 + 2*output) << WGM00);
            _SFR_IO8(addrTCC[1]) = ((prescal + 1) << CS00);
            break;
        case 2:
            _SFR_MEM8(addrTCC[0]) = ((1 + 2*output) << 0);
            _SFR_MEM8(addrTCC[1]) = ((prescal + 1) << 0);
            break;
        default:
            _SFR_MEM8(addrTCC[0]) = (1 << 0);
            _SFR_MEM8(addrTCC[1]) =(output << 3) | ((prescal + 1) << 0);
            break;
        }
    }
}

void EasyPwm::setCounterReg(uint8_t OCRx, uint8_t val){
    this->init();

    switch(timerNum){
    case 0:
        _SFR_IO8(addrOut[OCRx]) = (val & 0xFF);
        break;
    
    case 2:
        _SFR_MEM8(addrOut[OCRx]) = (val & 0xFF);
        break;
    
    default:
        _SFR_MEM16(addrOut[OCRx]) = (val & 0xFF);
        break;
    }
}

void EasyPwm::setOutput(uint8_t num, bool state, bool invers = false){
    this->init();

    switch(timerNum){
    case 0:
        if(state == true){
            _SFR_IO8(addrDataDir[num]) |= (1 << addrDataPin[num]);
            _SFR_IO8(addrTCC[0]) |= ((2 + invers) << (6 - 2*num));
        } else { 
            _SFR_IO8(addrDataDir[num]) &= ~(1 << addrDataPin[num]);
            _SFR_IO8(addrTCC[0]) &= ~(3 << (6 - 2*num));
        }

        break;
    case 2:
        if(state == true){
            if(num == 0)
                _SFR_IO8(addrDataDir[num]) |= (1 << addrDataPin[num]);
            else
                _SFR_MEM8(addrDataDir[num]) |= (1 << addrDataPin[num]);

            _SFR_MEM8(addrTCC[0]) |= ((2 + invers) << (6 - 2*num));
        } else { 
            if(num == 0)
                _SFR_IO8(addrDataDir[num]) &= ~(1 << addrDataPin[num]);
            else
                _SFR_MEM8(addrDataDir[num]) &= ~(1 << addrDataPin[num]);

            _SFR_MEM8(addrTCC[0]) &= ~(3 << (6 - 2*num));
        }

        break;
    default:
        if(!((timerNum == 4) || (timerNum == 5))){
            if(state == true){
                _SFR_IO8(addrDataDir[num]) |= (1 << addrDataPin[num]);
                _SFR_MEM8(addrTCC[0]) |= ((2 + invers) << (6 - 2*num));
            } else {
                _SFR_IO8(addrDataDir[num]) &= ~(1 << addrDataPin[num]);
                _SFR_MEM8(addrTCC[0]) &= ~(3 << (6 - 2*num));
            }
        } else {
            if(state == true){
                _SFR_MEM8(addrDataDir[num]) |= (1 << addrDataPin[num]);
                _SFR_MEM8(addrTCC[0]) |= ((2 + invers) << (6 - 2*num));
            } else {
                _SFR_MEM8(addrDataDir[num]) &= ~(1 << addrDataPin[num]);
                _SFR_MEM8(addrTCC[0]) &= ~(3 << (6 - 2*num));
            }
        }

        break;
    }
}

#endif