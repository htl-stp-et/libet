#pragma once
#include<Arduino.h>

#ifdef __AVR_ATmega2560__

class EasyPwm{
    public:
        EasyPwm(uint8_t timerNumb, uint8_t outputType, uint8_t prescalSet);
        void setCounterReg(uint8_t OCRx, uint8_t val);
        void setOutput(uint8_t num, bool state, bool invers = false);

    private:
        uint16_t addrTCC[2];
        uint8_t timerNum;
        uint16_t addrOut[3];
        uint16_t addrDataDir[3];
        uint8_t addrDataPin[3];
        bool isInitialized = false;
        uint8_t output;
        uint8_t prescal;
        void init();
        
};

#endif

#if defined (__arm__) && defined (__SAM3X8E__)

class EasyPwm{
    public:
        EasyPwm(uint8_t timer, uint8_t outputType, uint32_t frequency);
        void setFrequency(uint32_t frequ);
        void setDutyCyle(uint64_t percentage);
        void pwmOnOff(bool state);
        void setSync(bool state);
        void startSynced();

    private:
        uint32_t currentFrequency;
        uint32_t currentDutyCycle;
        uint8_t timerNum;    
        bool currentOutputType;           
};

#endif