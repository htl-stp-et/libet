#pragma once

#include<Arduino.h>
#include<LiquidCrystal.h>

extern LiquidCrystal lcd;

int powersOf(double powersOfThis);

String valToString(double value);

class ScreenSet{
    public:
        void screenUpdate();
        ScreenSet(uint8_t rs, uint8_t rw, uint8_t enable, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);

    private:
        uint8_t rsPin;
        uint8_t enaPin;
        uint8_t d4Pin;
        uint8_t d5Pin;
        uint8_t d6Pin;
        uint8_t d7Pin;
};

class EasyText{
    public:
        String text;
        uint8_t typeID;
        EasyText(String txt);
        uint8_t gruppenID;
        EasyText* next;
        EasyText* above;
        EasyText* below;
        void setGroup(uint8_t group);
        void setNext(EasyText& element);
        void setTop(EasyText& element);
        void setBelow(EasyText& element);
        static void nextOne();
        static void up();
        static void down();

    friend class Screenset;
};

class EasyParam: public EasyText{
    public:
        double paramValue;
        int8_t zehner;
        uint64_t lastPress;
        String unit;
        String displayValue;
        bool editParam;
        bool editDigit;
        explicit EasyParam(String txt, double val, String unit);
};

class EasyBool: public EasyText{
    public:
        bool state;
        explicit EasyBool(String txt, bool state);
};

extern EasyText* currentElement;


