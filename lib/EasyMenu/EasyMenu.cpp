#include <EasyMenu.hpp>
  
LiquidCrystal lcd(23, 25, 27, 29, 31, 33);
uint8_t cursorPosReal;
int8_t cursorPosZehner;

EasyText* currentElement = nullptr;
EasyParam* currentParam = nullptr;
EasyBool* currentBool = nullptr;

int powersOf(double powersOfThis){
    double calcWithThis = abs(powersOfThis);
    
    return log10(calcWithThis);
}

String valToString(double value){
    if (currentElement->typeID != 1) {
        return "";
    }
    currentParam = static_cast<EasyParam*>(currentElement);
    // currentBool = static_cast<EasyBool*>(currentElement);

    String outputString;
    int zehner = powersOf(value);

    if(value < 0){
        outputString += "-";
    }

    value = abs(value);

    if((zehner >= 3) && (zehner < 6)){
            currentParam->zehner = 3;
            outputString += (String(value/1000, 3) + "k");
    } else if((zehner >= 6) ){
            currentParam->zehner = 6;
            outputString += (String(value/1000000, 3) + "M");
    } else if((zehner <= -1) && (zehner >= -3)){
            currentParam->zehner = -3;
            outputString += (String(value*1000, 3) + "m");
    } else if((zehner <= -4) && (zehner >= -6)){
            currentParam->zehner = -6;
            outputString += (String(value*1000000, 3) + "u");
    } else if((zehner <= -7) && (zehner >= -9)){
            currentParam->zehner = -9;
            outputString += (String(value*1000000000, 3) + "n");
    } else {
            currentParam->zehner = 0;
            outputString += String(value, 3);
    }

    return outputString;
}

void EasyText::setGroup(uint8_t group){
    gruppenID = group;
}

void EasyText::setNext(EasyText& element){
    next = &element;
}

void EasyText::setTop(EasyText& element){
    above = &element;
    
    element.below = this;
}

void EasyText::setBelow(EasyText& element){
    below = &element;
    
    element.above = this;    
}

ScreenSet::ScreenSet(uint8_t rs, uint8_t rw, uint8_t enable, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7){    
    lcd.init(1, rs, 255, enable, d4, d5, d6, d7, 0, 0, 0, 0);

    pinMode(rw, OUTPUT);
    digitalWrite(rw, LOW);

    lcd.begin(16, 2);  
}

void ScreenSet::screenUpdate(){

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(">");
    lcd.setCursor(1, 0);
    lcd.print(currentElement->text);

    switch(currentElement->typeID){
        case 0:
            lcd.setCursor(1, 1);
            if(currentElement->below != nullptr)
                lcd.print(currentElement->below->text);
            break;
        case 1:
            currentParam = static_cast<EasyParam*>(currentElement);
            currentParam->displayValue = valToString(currentParam->paramValue);
            lcd.print("=");
            lcd.setCursor((16-currentParam->displayValue.length()-currentParam->unit.length()), 1);
            lcd.print(currentParam->displayValue);
            lcd.setCursor(16-currentParam->unit.length(), 1);
            lcd.print(currentParam->unit);
            break;
        case 2:
            currentBool = static_cast<EasyBool*>(currentElement);
            lcd.setCursor(currentBool->text.length()+1, 0);
            lcd.print(":");
            lcd.setCursor(13, 1);
            if(currentBool->state)
                lcd.print("On");
            else
                lcd.print("Off");

            break;
    }

    lcd.setCursor(cursorPosReal, 1);
}

EasyText::EasyText(String txt){
    text = txt;

    typeID = 0;
    next = nullptr;
    above = nullptr;
    below = nullptr;

    if(currentElement == nullptr)
        currentElement = this;
}

EasyParam::EasyParam(String txt, double val, String unit):EasyText{txt}{    
    paramValue = val;
    this->unit = unit;

    typeID = 1;

    if(currentElement == nullptr)
        currentElement = this;
}

EasyBool::EasyBool(String txt, bool state):EasyText{txt}{
    this->state = state;

    typeID = 2;

    if(currentElement == nullptr)
        currentElement = this;

}

void EasyText::nextOne(){
    uint64_t time;


    if(currentElement->typeID == 0) {
        if(currentElement->next != nullptr)
            currentElement = currentElement->next;
    } else if(currentElement->typeID == 1){
        currentParam = static_cast<EasyParam*>(currentElement);
        if(!currentParam->editParam){
            currentParam->editParam = true;

            cursorPosReal = (16-currentParam->displayValue.length()-currentParam->unit.length());

            if(currentParam->paramValue < 0)
                cursorPosReal = (17-currentParam->displayValue.length()-currentParam->unit.length());
                

            cursorPosZehner = powersOf(currentParam->paramValue) - currentParam->zehner;

            lcd.setCursor(cursorPosReal, 1);
            lcd.blink();

        }else{

            time = millis();
            if((time - currentParam->lastPress) < 500){
                lcd.noBlink();
                currentParam->editParam = false;
                currentParam->editDigit = false;
            }else
                currentParam->editDigit ^= true;

            currentParam->lastPress = time;
        }
    } else if(currentElement->typeID == 2){
        currentBool = static_cast<EasyBool*>(currentElement);
        currentBool->state ^= true;
    }
}

void EasyText::up(){
    bool prefix = 1;
    
    if(currentElement->typeID == 1){
        currentParam = static_cast<EasyParam*>(currentElement);

        if(currentParam->editParam){
            if(currentParam->editDigit)
                currentParam->paramValue += pow(10, (currentParam->zehner + cursorPosZehner));
            else{
                cursorPosReal--;
                cursorPosZehner++;

                if((currentParam->paramValue >= 0) && (currentParam->paramValue < 1000))
                    prefix = 0;

                if(cursorPosReal == (16-4-prefix-currentParam->unit.length()))
                    cursorPosReal--;
            }

        }else{
            if(currentParam->above != nullptr)
                currentElement = currentParam->above;
        }
    }else{        
            if(currentElement->above != nullptr)
                currentElement = currentElement->above;
    }

}

void EasyText::down(){
    bool prefix = 1;
    
    if(currentElement->typeID == 1){
        currentParam = static_cast<EasyParam*>(currentElement);

        if(currentParam->editParam){
            if(currentParam->editDigit)
                currentParam->paramValue -= pow(10, (currentParam->zehner + cursorPosZehner));

            else{            
                cursorPosReal++;
                cursorPosZehner--;

                if((currentParam->paramValue >= 0) && (currentParam->paramValue < 1000))
                    prefix = 0;

                if(cursorPosReal == (16-4-prefix-currentParam->unit.length()))
                    cursorPosReal++;
            }

        }else{
            if(currentParam->below != nullptr)
                currentElement = currentParam->below;    
        }
    }else{
        if(currentElement->below != nullptr)
            currentElement = currentElement->below;  
    }

    
}