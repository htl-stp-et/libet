#include <Arduino.h>

#define BUFFERSIZE 16

class EasyUart{
    private:
        uint8_t sendBuffer[BUFFERSIZE];
        uint8_t receiveBuffer[BUFFERSIZE];

        uint8_t readPointerSoll;
        uint8_t readPointerIst;

        uint8_t sendPointerSoll;
        uint8_t sendPointerIst;

        uint8_t uartNum;
        bool stBit;
        bool polarity;
        bool transmitting;

    public:
        EasyUart(uint8_t num, bool stopBits, bool cpol);
        void toTransferBuffer(uint8_t *sendArray, uint8_t length);
        void readReceiveBuffer(uint8_t *writeArray, uint8_t length);
        void setBaud(uint32_t baud, bool doubleSpeed);
        void transmit();
        void receive();
        uint8_t getReceived();


};