#include <EasyUart.hpp>

/**
 *  Array mit Adressen der UDR-Register der Uarts
 */
uint16_t UDRx[] = {
    0xC6,
    0xCE,
    0xD6,
    0x136
};
uint16_t UCSRxA[] = {
    0xC0,
    0xC8,
    0xD0,
    0x130
};
uint16_t UCSRxB[] = {
    0xC1,
    0xC9,
    0xD1,
    0x131
};
uint16_t UCSRxC[] = {
    0xC2,
    0xCA,
    0xD2,
    0x132
};
//MEM16
uint16_t UBRRx[] = {
    0xC4,
    0xCC,
    0xD4,
    0x134
};

EasyUart::EasyUart(uint8_t num, bool stopBits, bool cpol){
    uartNum = num;

    readPointerSoll = 0;
    readPointerIst = 0;
    sendPointerSoll = 0;
    sendPointerIst = 0;

    stBit = stopBits;
    polarity = cpol;

    sei();
}

void EasyUart::setBaud(uint32_t baud, bool doubleSpeed){
    uint16_t baudBits;

    _SFR_MEM8(UCSRxA[uartNum]) = (doubleSpeed << 1);
    _SFR_MEM8(UCSRxB[uartNum]) = (3 << 3) | (3 << 6);
    _SFR_MEM8(UCSRxC[uartNum]) = (stBit << 3) | (3 << 1) | (polarity << 0);

    if(doubleSpeed)
        baudBits = uint16_t(16000000.0/(8.0 * double(baud))) - 1;
    else
        baudBits = uint16_t(16000000.0/(16.0 * double(baud))) - 1;

    _SFR_MEM16(UBRRx[uartNum]) = baudBits;
}

void EasyUart::toTransferBuffer(uint8_t *sendArray, uint8_t length){
    for(uint8_t i = 0; i < length; i++){        
        sendBuffer[sendPointerSoll] = sendArray[i]; 

        sendPointerSoll = (sendPointerSoll + 1) & 0xF;
    }    

    if(!transmitting){
        _SFR_MEM16(UDRx[uartNum]) = sendBuffer[sendPointerIst];
        transmitting = true;
    }
}

void EasyUart::transmit(){
    sendPointerIst = (sendPointerIst + 1) & 0xF;
    if(sendPointerIst != sendPointerSoll){
        _SFR_MEM16(UDRx[uartNum]) = sendBuffer[sendPointerIst];
    }else{
        transmitting = false;
    }
}

uint8_t EasyUart::getReceived(){
    return readPointerSoll - readPointerIst;
}

void EasyUart::readReceiveBuffer(uint8_t *writeArray, uint8_t length){
    for(uint8_t i = 0; i < length; i++){
        writeArray[i] = receiveBuffer[readPointerIst];
        readPointerIst = (readPointerIst + 1) & 0xF;
    }
}

void EasyUart::receive(){
    receiveBuffer[readPointerSoll] = _SFR_MEM16(UDRx[uartNum]);
    
    readPointerSoll = (readPointerSoll + 1) & 0xF;
}