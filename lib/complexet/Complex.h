/**
 * @file Complex.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Stellt 2 Datentypen mit jeweils 32 und 64 Bit Genauigkeit für Komplexe Zahlen bereit
 * @version 1.0
 * @date 2020-02-21
 * 
 * Stellt die Variablendefinitionen `complex32_t` und `complex64_t` bereit.
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef COMPLEX_H_
#define COMPLEX_H_

#include "./fixpoints.h"

/**
 * @brief Eine Klasse zum Rechnen mit Komplexen Zahlen
 * 
 * @tparam fixpoint_t 
 */
template<typename fixpoint_t>
class Complex {
public:
    fixpoint_t real_;
    fixpoint_t imag_;
    
    /**
     * @brief Erzeugt eine Komplexe Zahl
     * 
     * @param real 
     * @param imag 
     */
    Complex(fixpoint_t real = 0.0, fixpoint_t imag = 0.0) : real_(real), imag_(imag) {}

    fixpoint_t abs() {
        return (real_.pow(2) + imag_.pow(2)).sqrt();
    }

    Complex operator+(Complex a) {
        return Complex(real_ + a.real_, imag_ + a.imag_);
    }

    Complex operator-() {
        return Complex(-real_, -imag_);
    }

    Complex operator-(Complex a) {
        return *this + (-a);
    }

    Complex operator*(Complex a) {
        return Complex(real_ * a.real_ - imag_ * a.imag_, real_*a.imag_ + imag_*a.real_);
    }

    Complex operator*(fixpoint_t a) {
        return Complex(real_ * a, imag_ * a);
    }

    Complex operator/(Complex a) {
        int64_t re = a.real_.value_;
        int64_t im = a.imag_.value_;

        int64_t N = re*re + im*im;

        if (N == 0) {
            return Complex(fixpoint_t::INF_, 0);
        }

        re = re<<fixpoint_t::KOMMA_;
        im = (-im)<<fixpoint_t::KOMMA_;

        int64_t ergre = (re*real_.value_ - im*imag_.value_)/N;
        int64_t ergim = (re*imag_.value_ + im*real_.value_)/N;

        if (ergre > fixpoint_t::INF_) ergre = fixpoint_t::INF_;
        if (ergre < fixpoint_t::NINF_) ergre = fixpoint_t::NINF_;
        if (ergim > fixpoint_t::INF_) ergim = fixpoint_t::INF_;
        if (ergim < fixpoint_t::NINF_) ergim = fixpoint_t::NINF_;

        return Complex(fixpoint_t(ergre, true), fixpoint_t(ergim, true));
    }

    Complex operator/(fixpoint_t a) {
        return Complex(real_ / a, imag_ / a);
    }

    Complex inv() {
        int64_t re = real_.value_;
        int64_t im = imag_.value_;

        int64_t N = re*re + im*im;

        if (N == 0) {
            return Complex(fixpoint_t::INF_, 0);
        }

        re = (re<<(fixpoint_t::KOMMA_ * 2))/N;
        im = ((-im)<<(fixpoint_t::KOMMA_*2))/N;

        if (re > fixpoint_t::INF_)  re = fixpoint_t::INF_;
        if (re < fixpoint_t::NINF_) re = fixpoint_t::NINF_;
        if (im > fixpoint_t::INF_)  im = fixpoint_t::INF_;
        if (im < fixpoint_t::NINF_) im = fixpoint_t::NINF_;

        return Complex(fixpoint_t(re, true), fixpoint_t(im, true));
    }

    Complex conj() {
        return Complex(real_, -imag_);
    }

    Complex normalize() {
        return *this / this->abs();
    }
};

using complex32_t = Complex<fixpoint32_t>;
using complex64_t = Complex<fixpoint64_t>;

#endif