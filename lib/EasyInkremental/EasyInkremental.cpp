#include <EasyInkremental.hpp>

/*
 * Function pointer for user functions
 */
void (*funcPtrA)();
void (*funcPtrB)();
void (*funcPtrSW)();

/*
 * Varaibles for activated interrupts
 */ 
int8_t vectA;
int8_t vectB;
int8_t vectSW;

uint8_t readCount = 0;

bool alreadyReadA = false;
bool alreadyReadB = false;

EasyInkremental::EasyInkremental(int8_t intANum, int8_t intBNum, int8_t intSWNum){
    intA = intANum;
    intB = intBNum;
    intSW = intSWNum;

    if(intANum <= 3){
        DDRD &= ~(1 << intANum);
        PORTD &= ~(1 << intANum);
        AdrrPinA = 0x09;
    }else{  
        DDRE &= ~(1 << intANum);
        PORTE &= ~(1 << intANum);
        AdrrPinA = 0x0C;
    }

    if(intBNum <= 3){
        DDRD &= ~(1 << intBNum);
        PORTD &= ~(1 << intBNum);
        AdrrPinB = 0x09;
    }else{
        DDRE &= ~(1 << intBNum);
        PORTE &= ~(1 << intBNum);
        AdrrPinB = 0x0C;
    }

    if(intSWNum <= 3){
        DDRD &= ~(1 << intSWNum);
        PORTD &= ~(1 << intSWNum);
    }else{
        DDRE &= ~(1 << intSWNum);
        PORTE &= ~(1 << intSWNum);
    }
    
    if(intANum <= 3)
        EICRA |= (2 << (intANum * 2));
    else
        EICRB |= (2 << (intANum * 2));

    if(intBNum <= 3)
        EICRA |= (3 << (intBNum * 2));
    else
        EICRB |= (3 << (intBNum * 2));

    if(intSWNum <= 3)
        EICRA |= (2 << (intSWNum * 2));
    else
        EICRB |= (2 << (intSWNum * 2));
    
    EIMSK |= (1 << intANum) | (1 << intBNum) | (1 << intSWNum);

    vectA = (intANum + 1);
    vectB = (intBNum + 1);
    vectSW = (intSWNum + 1);
}

void EasyInkremental::intEvA(){
    aHigh = (_SFR_IO8(AdrrPinB) & (1 << intB));
}

void EasyInkremental::intEvB(){
    bHigh = (_SFR_IO8(AdrrPinA) & (1 << intA));

    if(bHigh == aHigh){
        if((bHigh & aHigh) == 1)
            funcPtrA();
        else
            funcPtrB();
    } 
}

void EasyInkremental::setFuncClkWise(void runFunc()){
    funcPtrA = runFunc;
}

void EasyInkremental::setFuncCclkWise(void runFunc()){
    funcPtrB = runFunc;
}

void EasyInkremental::setFuncSwitch(void runFunc()){
    funcPtrSW = runFunc;
}