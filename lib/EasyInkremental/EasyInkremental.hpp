#include <Arduino.h>
#include <avr/io.h>
#include <avr/interrupt.h>

class EasyInkremental {
private:
    uint8_t AdrrPinA;
    uint8_t AdrrPinB;
    uint8_t intA;
    uint8_t intB;
    uint8_t intSW;
    bool bHigh;
    bool aHigh;
    bool lastInt;

public:
    EasyInkremental(int8_t intANum, int8_t intBNum, int8_t intSWNum);
    void intEvA();
    void intEvB();
    void setFuncClkWise(void runFunc());
    void setFuncCclkWise(void runFunc());
    void setFuncSwitch(void runFunc());
};




