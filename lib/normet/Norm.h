/**
 * @file Norm.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Stellt eine Klasse fürs einfache Normieren von Werten bereit.
 * @version 1.0
 * @date 2020-02-21
 * 
 * Die Werte können durch das Angeben eines Offsets und einer Bezugsgröße einfach normiert werde.
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef NORM_H
#define NORM_H

#include "./fp.h"
#include "./fp32.h"
#include <inttypes.h>

template<typename type_t>
class Norm {
private:
    using norm_t = Fixpoint<int32_t, int64_t, 29>;
    norm_t normiert_;
    type_t bezugswert_;
    type_t offset_;

    /**
     * @brief Wandelt die normierte Zahl in eine 32-Bit Fixpoint um.
     * 
     * @param a                 Normierte Zahl
     * @return fixpoint32_t     32-Bit Fixpoint Repräsentation der Normierten Zahl
     */
    fixpoint32_t toFP32(norm_t a) {
        return fixpoint32_t(a.value_ >> (norm_t::KOMMA_ - fixpoint32_t::KOMMA_), true);
    }

    /**
     * @brief Wandelt eine 32-Bit Fixpoint in eine normierte Zahl um.
     * 
     * @param a         32-Bit Fixpoint
     * @return norm_t   Normierte Repräsentation dieser Zahl
     */
    norm_t toNORM(fixpoint32_t a) {
        return norm_t(a.value_ << (norm_t::KOMMA_ - fixpoint32_t::KOMMA_), true);
    }

public:
    Norm(): bezugswert_(0), offset_(0) {};

    /**
     * @brief Erstellt ein neues Norm Objekt
     * 
     * @param wert          Aktueller unnormierter Wert
     * @param bezugswert    Bezugswert der Zahl, entspricht einer normierten 1
     * @param offset        Offset der Zahl, entspricht einer normierten 0
     */
    Norm(type_t wert, type_t bezugswert, type_t offset = 0): bezugswert_(bezugswert), offset_(offset) {
        normiert_.value_ = (fixpoint32_t(int(wert - offset_)) / fixpoint32_t(int(bezugswert_ - offset_))).value_ << (norm_t::KOMMA_ - fixpoint32_t::KOMMA_);
    }

    /**
     * @brief Setzt die Parameter des Norm-Objektes
     * 
     * @param wert          Aktueller unnormierter Wert
     * @param bezugswert    Bezugswert der Zahl, entspricht einer normierten 1
     * @param offset        Offset der Zahl, entspricht einer normierten 0
     */
    void set(type_t wert, type_t bezugswert, type_t offset){
        normiert_.value_ = (fixpoint32_t(int(wert - offset)) / fixpoint32_t(int(bezugswert - offset))).value_ << (norm_t::KOMMA_ - fixpoint32_t::KOMMA_);    
        bezugswert_ = bezugswert;
        offset_ = offset;
    }

    /**
     * @brief Updated den normierten Wert
     * 
     * @param newValue      Neuer unnormierter Wert
     */
    void update(type_t newValue) {
        normiert_.value_ = (fixpoint32_t(int(newValue - offset_)) / fixpoint32_t(int(bezugswert_ - offset_))).value_ << (norm_t::KOMMA_ - fixpoint32_t::KOMMA_);
    }

    /**
     * @brief Updated den normierten Wert mit einer anderen Bezugsbasis
     * 
     * @param newValue      Neuer unnormierter Wert
     * @param bezugswert    Bezugswert der Zahl, entspricht einer normierten 1
     * @param offset        Offset der Zahl, entspricht einer normierten 0
     */
    void update(type_t newValue, type_t bezugswert, type_t offset) {
        normiert_.value_ = (fixpoint32_t(int(newValue - offset)) / fixpoint32_t(int(bezugswert - offset))).value_ << (norm_t::KOMMA_ - fixpoint32_t::KOMMA_);
    }

    /**
     * @brief Gibt den normierten Wert zurück
     * 
     * @return norm_t       Normierter Wert
     */
    norm_t getNormated() {
        return normiert_;
    }

    /**
     * @brief Gibt den normierten Wert als eine 32-Bit Fixpoint zurück
     * 
     * @return fixpoint32_t Normierter Wert in Fixpointrepräsentation
     */
    fixpoint32_t getNormatedFixpoint() {
        return toFP32(normiert_);
    }

    /**
     * @brief Setzt den normierten Wert direkt
     * 
     * @param value     Neuer normierter Wert
     */
    void setNormated(norm_t value) {
        normiert_ = value;
    }

    /**
     * @brief Gibt den denormierten Wert zurück
     * 
     * @return type_t   Denormierter Wert
     */
    type_t denorm() {
        return denorm(bezugswert_, offset_);
    }

    /**
     * @brief Gibt den denormierten Wert auf einen anderen Bezugswert bezogen zurück
     * 
     * @param bezugswert    Bezugswert der Zahl, entspricht einer normierten 1
     * @param offset        Offset der Zahl, entspricht einer normierten 0
     * @return type_t       Denormierte Zahl
     */
    type_t denorm(type_t bezugswert, type_t offset = 0) {
        return type_t((toFP32(normiert_) * fixpoint32_t(int(bezugswert - offset))).value_ >> fixpoint32_t::KOMMA_) + offset;
    }
};

#endif
