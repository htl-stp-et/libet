/**
 * @file Norm.cpp
 * @author Andreas Kohler
 * @brief Definiert die benötigte Fixpoint Variable
 * @version 1.0
 * @date 2020-02-21
 * 
 * Diese Fixpoint-Variable ist 32 Bit groß.
 * Für die Nachkommastellen sind 29 Bit reserviert.
 * Für das Vorzeichen wird 1 Bit benötigt.
 * Daher bleiben für die Vorkommastellen 2 Bit übrig.
 * 
 * Es lassen sich Zahlen zwischen 3.9999 und -3.9999 darstellen.
 * 
 * @copyright Copyright (c) 2020
 *
 */

#include "./Norm.h"

template<> int Fixpoint<int32_t, int64_t, 29>::KOMMA_ = 29;
template<> int32_t Fixpoint<int32_t, int64_t, 29>::INF_ = 0x7FFFFFFF;
template<> int32_t Fixpoint<int32_t, int64_t, 29>::NINF_ = 0x80000001;
template<> int32_t Fixpoint<int32_t, int64_t, 29>::NAN_ = 0x80000000;
template<> int32_t Fixpoint<int32_t, int64_t, 29>::MAX_ = (int32_t(1) << (32 - 29 - 1))-1;
template<> int32_t Fixpoint<int32_t, int64_t, 29>::MIN_ = -Fixpoint<int32_t, int64_t, 29>::MAX_;