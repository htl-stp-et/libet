/**
 * @file Winkel.cpp
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Implementiert Winkelfunktionen
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "./Winkel.h"

fixpoint32_t sin_[17] = {
	0,
	0.0980171403295606,
	0.1950903220161283,
	0.2902846772544623,
	0.3826834323650898,
	0.4713967368259976,
	0.5555702330196022,
	0.6343932841636455,
	0.7071067811865475,
	0.7730104533627369,
	0.8314696123025452,
	0.8819212643483549,
	0.9238795325112867,
	0.9569403357322089,
	0.9807852804032304,
	0.9951847266721968,
	1
};
fixpoint32_t tan_[8] = {
	0,
	0.1989123673796580,
	0.3033466836073424,
	0.4142135623730950,
	0.5345111359507916,
	0.6681786379192989,
	0.8206787908286603,
	1
};

/**
 * @brief Erzeugt eine neue Winkel Zahl
 * 
 * @param value 		Wert
 * @param einheit 		RAD oder GRAD
 */
Winkel::Winkel(double value, int einheit) {
	switch (einheit) {
		case RAD:  value_ = value*32768.0/dPI;   break;
		case GRAD: value_ = value*32768.0/180.0; break;
	}
}

/**
 * @brief Erzeugt eine neue Winkel Zahl
 * 
 * @param value 		Wert
 * @param einheit 		RAD oder GRAD
 */
Winkel::Winkel(fixpoint32_t value, int einheit) {
	switch (einheit) {
		case RAD:  value_ = (value*fixpoint32_t(32768, false)/180).value_ >> fixpoint32_t::KOMMA_; break;
		case GRAD: value_ = (value*fixpoint32_t(32768, false)/fixpoint32_t(dPI)).value_ >> fixpoint32_t::KOMMA_; break;
	}
}

/**
 * @brief Gibt den aktuellen Wert in Grad zurück
 * 
 * @return fixpoint32_t		Aktueller Grad Wert 
 */
fixpoint32_t Winkel::toGrad() {
    return fixpoint32_t(value_ * 180 / 32768, false);
}

/**
 * @brief Gibt den aktuellen Wert in Radianten zurück
 * 
 * @return fixpoint32_t 	Aktueller Radianten Wert
 */
fixpoint32_t Winkel::toRad() {
    return fixpoint32_t((int32_t)value_, false) * fixpoint32_t(dPI) / fixpoint32_t(32768, false);
}

/**
 * @brief Berechnet den Sinuswert der Variable
 * 
 * @return fixpoint32_t 	Ergebnis
 */
fixpoint32_t Winkel::sin() {
    static uint16_t mask = (2<<13)-1;
    int32_t k, g;
    int v = 1;
    switch(value_>>14) {
    case 2: // 3. Quadrant
        v = -1;
        //* no break!
    case 0: // 1. Quadrant
        k = (value_ & mask)>>10;
        g = k+1;
        break;
    case 3: // 4. Quadrant
        v = -1;
        //* no break!
    case 1: // 2. Quadrant
        g = 15-((value_ & mask)>>10);
        k = g + 1;
        break;
    default: return 0;
    }

    // Interpolation zwischen größerem Winkel g und kleinerem Winkel k
    fixpoint32_t e;
    #if komma < 10
        e = fixpoint32_t((value_&(mask >> 4) << (fixpoint32_t::KOMMA_-10)), true);
    #else
        e = fixpoint32_t((value_&(mask >> 4) >> (10 - fixpoint32_t::KOMMA_)), true);
    #endif

    e = sin_[k] + (sin_[g]-sin_[k])*e;
    return e*v;
}

/**
 * @brief Berechnet den Cosinuswert der Variable
 * 
 * @return fixpoint32_t 	Ergebnis
 */
fixpoint32_t Winkel::cos() {
    Winkel w;
    w.value_ = W_90 - value_;
    return w.sin();
}

/**
 * @brief Berechnet den Tangenswert der Variable
 * 
 * @return fixpoint32_t 	Ergebnis
 */
fixpoint32_t Winkel::tan() {
    return sin() / cos();
}

/**
 * @brief Berechnet den Winkel mittels Arcustangens
 * 
 * @param y 		Gegenkathete
 * @param x 		Ankathete
 * @return Winkel 	Winkel
 */
Winkel Winkel::atan2(fixpoint32_t y, fixpoint32_t x) {
    if (x>=0) {
		if (y>=0) { // 1.Quadrant
			int32_t i,l=0;
			if (x==0 || y==fixpoint32_t(fixpoint32_t::INF_, true)) {
				winkel_t w;
				w.value_ = W_90;
				return w;
			}
			fixpoint32_t qv;
			int32_t G45 = 0;
			if (y>x) { // Winkel > 45 °
				G45=1;
				qv = x / y;
			} else qv = y / x;
			for (i=0;i<8;i++) if (qv>=tan_[i]) l=i;
			if (l==8) return W_45;
			uint16_t e;
			e = l<<(10);
			fixpoint32_t de = fixpoint32_t(((qv-tan_[l]).value_<<(10))/(tan_[l+1]-tan_[l]).value_, true);
			e = e + de.value_;
			if (G45) e = W_90-e;
            winkel_t ret;
            ret.value_ = e;
			return ret;
		} else {    // 4.Quadrant
            winkel_t ret;
            ret.value_ = -atan2(-y,x).value_;
			return ret;
		}
	} else {
		if (y>=0) { // 2.Quadrant
            winkel_t ret;
            ret.value_ = W_180-atan2(y,-x).value_;
			return ret;
		} else {    // 3.Quadrant
            winkel_t ret;
            ret.value_ = atan2(-y,-x).value_ + W_180;
			return ret;
		}
	}
}