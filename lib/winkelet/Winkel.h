/**
 * @file Winkel.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Definiert Winkel Klasse zur einfachen Verwendung von Winkelfunktionen.
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef WINKEL_H
#define WINKEL_H

#include "./fp32.h"
#include <inttypes.h>

#define       W_0   0
#define       WMask 0xFFFF
#define       W_90  0x4000
#define       W_1   0x0182
#define       W_30  0x1555
#define       W_45  0x2000
#define       W_60  0x2AAA
#define       W_120 0x5555
#define       W_180 0x8000
#define       W_210 0x9555
#define       W_240 0xAAAA
#define       W_270 0xC000
#define       W_300 0xD555

// Double Konstante
#define dPI  	3.141592653589793238
#define SQR3 	1.732050807568877
#define SQR2 	1.414213562373095
#define SQR3D2 	0.8660254037844386	// = sqrt(3)/2
#define EDSQR3 	0.5773502691896258	// = 1/sqrt(3)
#define ZDSQR3 	1.154700538379252	// = 2/sqrt(3)
#define EDSQR2 	0.7071067811865475	// = 1/sqrt(2)

#define GRAD 0
#define RAD  1

class Winkel {
public:
    uint16_t value_;
    Winkel(): value_(0) {}
    Winkel(double value, int einheit = GRAD);
    Winkel(fixpoint32_t value, int einheit = GRAD);

    fixpoint32_t toGrad();
    fixpoint32_t toRad();

    fixpoint32_t sin();
    fixpoint32_t cos();
    fixpoint32_t tan();

    static Winkel atan2(fixpoint32_t y, fixpoint32_t x);
};

typedef Winkel winkel_t;

#endif