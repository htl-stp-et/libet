#ifndef DIFFERENZENGLEICHUNG_H_
#define DIFFERENZENGLEICHUNG_H_

// Maximale Ordnung der Differenzengleichung
#define DGL_N_MAX 10

// Pufferlänge in 2^BUFF_BIT
#define BUFF_BIT  4
#define BUFF_SIZE (1<<BUFF_BIT)
#define BUFF_MASK (BUFF_SIZE-1)

/**
 * @brief Beschreibt den Speicherbaustein, welcher für die DiffGlg. benötigt wird.
 * 
 * @tparam type_t Variablentyp des Buffers. Sinnvollerweiße entweder ein Fixpointdatentyp oder eine double.
 */
template<typename type_t>
class DGL_Buffer {
public:
    unsigned int pos_;
    type_t e[BUFF_SIZE];

    /**
     * @brief Erstellt ein neuen Buffer-Baustein
     * 
     */
    DGL_Buffer(): pos_(0) {}

    /**
     * @brief Gibt das nächste Element im Buffer aus und setzt die neue Position auf das Element.
     * 
     * @return type_t Eingestellter Typ des Puffers.
     */
    type_t inc() {
        ++pos_;
        pos_ &= BUFF_MASK;
        return e[pos_];
    }

    /**
     * @brief Gibt das Element an Position n zurück.
     * 
     * @param n Position, relativ zur derzeitigen Position.
     * @return type_t Eingestellter Typ des Puffers.
     */
    type_t getElement(int n) {
        return e[(pos_ - n) & BUFF_MASK];
    }

    /**
     * @brief Aktualisiert das derzeitige Buffer-Element und setzt die neue Position auf das Element.
     * 
     * @param element Das neue Buffer-Element
     */
    void push(type_t element) {
        e[pos_] = element;
        inc();
    }
};

/**
 * @brief Implementiert die Logik der Differenzengleichung.
 * 
 * @tparam type_t Variablentyp des Systems. Sinnvollerweiße entweder ein Fixpointdatentyp oder eine double.
 */
template<typename type_t>
class Differenzengleichung {
private:
    int n_;                     // Ordnung des Systems
    type_t a_[DGL_N_MAX];       // negative Faktoren des Nenners der z-Übertragungsfunktion
    type_t b_[DGL_N_MAX+1];     // Faktoren des Zählers der z-Übertragungsfunktion
    DGL_Buffer<type_t> inBuf;   // Speichert die letzten Eingangswerte
    DGL_Buffer<type_t> outBuf;  // Speichert die letzten Ausgangswerte
public:

    /**
     * @brief Erzeugt ein neues Differenzengleichung Objekt
     * 
     * @param n Ordnung des Systems
     * @param a Nenner der Differenzengleichung
     * @param b Zähler der Differenzengleichung
     */
    Differenzengleichung(int n, type_t *a, type_t *b) {
        if (n > DGL_N_MAX) n_ = DGL_N_MAX;
        else n_ = n;

        b_[0] = b[0];

        for (int i = 0; i < n_; ++i) {
            a_[i] = a[i];
            b_[i + 1] = b[i + 1];
        }
        for (int i = n_; i < DGL_N_MAX; ++i) {
            a_[i] = 0;
            b_[i + 1] = 0;
        }
    }

    /**
     * @brief Berechnet den nächsten Schritt
     * 
     * @param in Eingangswert
     * @return type_t Ausgangswert
     */
    type_t timestep(type_t in) {
        type_t out;

        out = b_[n_] * in;
        for (int i = 0; i < n_; ++i) {
            out += a_[i] * outBuf.getElement(n_ - i)
                +  b_[i] * inBuf.getElement(n_ - i);
        }

        outBuf.push(out);
        inBuf.push(in);

        return out;
    }

    /**
     * @brief Gibt die Ordnung des Systems zurück.
     * 
     * @return int Ordnung des Systems
     */
    int getOrdnung() { return n_; }

    /**
     * @brief Gibt den iten a-Faktor zurück
     * 
     * @param i 
     * @return type_t 
     */
    type_t getA(int i) {
        if (i == n_) return 1;
        if (i > n_) return 0;
        return -a_[i];
    }

    /**
     * @brief 
     * 
     * @param i 
     * @return type_t 
     */
    type_t getB(int i) {
        if (i > n_) return 0;
        return b_[i];
    }
};

#endif