/**
 * @file Grundglieder.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Stellt vorgefertigte Grundglieder zur Verfügung.
 * @version 1.0
 * @date 2020-02-21
 * 
 * Es sind folgende Grundglieder definiert:
 *  - P
 *  - PT1
 *  - D
 *  - DT1
 *  - PD
 *  - PDa
 *  - PDT1
 *  - PDT1a
 *  - PDT1b
 *  - D2
 *  - D2T
 *  - PD2s
 *  - PD2T
 *  - PT2
 *  - PT2s
 *  - I
 *  - PI
 *  - PIDT1
 *  - Allpass
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef GRUNDGLIEDER_H
#define GRUNDGLIEDER_H

#include "./DifferenzenGlg.h"

/**
 * P-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * G(s)= kp
 */
template<typename typ>
Differenzengleichung<typ> DGL_P(typ kp) {
    typ a[0] = {};
    typ b[1] = {kp};
    return Differenzengleichung<typ>(1, a, b);
}

/**
 * PT1-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * G(s)= kp/(1+sT1)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PT1(typ T, typ kp, typ T1) {
    typ a[1] = {T1/(T+T1)};
    typ b[2] = {0, kp*T/(T+T1)};
    return Differenzengleichung<typ>(1, a, b);
}

/**
 * D-Glied
 * T     Abtastzeit in s
 * Tv    Vorhaltezeit in s
 * G(s)= sTv
 */
template<typename typ>
Differenzengleichung<typ> DGL_D(typ T, typ Tv) {
    typ a[1] = {0};
    typ b[2] = {-Tv/T, Tv/T};
    return Differenzengleichung<typ>(1, a, b);
}

/** 
 * DT1-Glied
 * T     Abtastzeit in s
 * Tv    Vorhaltezeit in s
 * T1    Verzögerungszeitkonstante
 * G(s)= sTv/(1+sT1)
 */
template<typename typ>
Differenzengleichung<typ> DGL_DT1(typ T, typ Tv, typ T1) {
    typ a[1] = {T1/(T+T1)};
    typ b[2] = {-Tv/(T+T1), Tv/(T+T1)};
    return Differenzengleichung<typ>(1, a, b);
}

/** 
 * PD-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * Tv    Vorhaltezeit in s
 * G(s)= kp(1+sTv)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PD(typ T, typ kp, typ Tv) {
    typ a[1] = {0};
    typ b[2] = {-kp*Tv/T, kp*(T+Tv)/T};
    return Differenzengleichung<typ>(1, a, b);
}

/** 
 * PD-Glied Parallelstruktur
 * T     Abtastzeit in s
 * kp    Verstärkung
 * Tv    Vorhaltezeit in s
 * G(s)= kp+sTv
 */
template<typename typ>
Differenzengleichung<typ> DGL_PDa(typ T, typ kp, typ Tv) {
    typ a[1] = {0};
    typ b[2] = {-Tv/T, (kp*T+Tv)/T};
    return Differenzengleichung<typ>(1, a, b);
}

/** 
 * PDT1-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * Tv    Vorhaltezeit in s
 * T1    Verzögerungszeitkonstante
 * G(s)= kp(1+sTv)/(1+sT1)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PDT1(typ T, typ kp, typ Tv, typ T1) {
    typ a[1]={T1/(T+T1)};
	typ b[2]={-kp*Tv/(T+T1),kp*(T+Tv)/(T+T1)};
	return Differenzengleichung<typ>(1,a,b);
}

/** 
 * PDT1-Glied Alternative 1
 * T     Abtastzeit in s
 * kp    Verstärkung
 * Tv    Vorhaltezeit in s
 * T1    Verzögerungszeitkonstante
 * G(s)= (kp+sTv)/(1+sT1)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PDT1a(typ T, typ kp, typ Tv, typ T1) {
    typ a[1]={T1/(T+T1)};
	typ b[2]={-Tv/(T+T1),(kp*T+Tv)/(T+T1)};
	return Differenzengleichung<typ>(1,a,b);
}

/** 
 * PDT1-Glied Alternative 2
 * T     Abtastzeit in s
 * kp    Verstärkung
 * Tv    Vorhaltezeit in s
 * T1    Verzögerungszeitkonstante
 * G(s)= kp + sTv/(1+sT1)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PDT1b(typ T, typ kp, typ Tv, typ T1) {
    typ a[1]={T1/(T+T1)};
	typ b[2]={-(kp*T1+Tv)/(T+T1),(kp+Tv/(T+T1))};
	return Differenzengleichung<typ>(1,a,b);
}

/** 
 * D2-Glied
 * T     Abtastzeit in s
 * Tv    Vorhaltezeit in
 * G(s)= s^2Tv
 */
template<typename typ>
Differenzengleichung<typ> DGL_D2(typ T, typ Tv) {
    typ t = Tv*Tv/T/T;
	typ a[2]={0,0};
	typ b[3]={t,-2.0*t,t};
	return Differenzengleichung<typ>(2,a,b);
}

/** 
 * D2T-Glied
 * T     Abtastzeit in s
 * Tv    Vorhaltezeit in s
 * T1    Verzögerungszeitkonstante
 * G(s)= s^2Tv/(1+sT1)^2
 */
template<typename typ>
Differenzengleichung<typ> DGL_D2T(typ T, typ Tv, typ T1) {
    typ t = Tv*Tv/(T+T1)/(T+T1);
	typ a[2]={-T1*T1/(T+T1)/(T+T1),2.0*T1/(T+T1)};
	typ b[3]={t,-2.0*t,t};
	return Differenzengleichung<typ>(2,a,b);
}

/** 
 * PD2-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * D     Dämpfung
 * wn    natürlichen Kreisfrequenz
 * G(s)= kp*(1 + 2*D*Tv*s + Tv^2*s^2)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PD2s(typ T, typ kp, typ D, typ wn) {
    typ a[2]={0,0};
	typ b[3]={kp/wn/wn/T/T,-2.0*kp*(D+1/wn/T)/wn/T,kp*(1+1/wn/wn/T/T+2*D/wn/T)};
	return Differenzengleichung<typ>(2,a,b);
}

/** 
 * PD2T-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * D     Dämpfung
 * wn    natürlichen Kreisfrequenz
 * T1    Verzögerungszeitkonstante
 * G(s)= kp*(1 + 2*D*Tv*s + Tv^2*s^2)/(1+s*T1)^2
 */
template<typename typ>
Differenzengleichung<typ> DGL_PD2T(typ T, typ kp, typ D, typ wn,typ T1) {
    typ t = (T+T1)*(T+T1);
	typ a[2]={-T1*T1/t,2*T1/(T+T1)};
	typ b[3]={kp/wn/wn/t,-2.0*kp*(D/wn*T+1/wn/wn)/t,kp*(T*T+1/wn/wn+2*D/wn*T)/t};
	return Differenzengleichung<typ>(2,a,b);
}

/** 
 * PT2-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * T1    Zeitkonstante 1
 * T2    Zeitkonstante 2
 * G(s)= kp/(1 + s*T1)/(1+s*T2)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PT2(typ T, typ kp, typ T1, typ T2) {
    typ t = (T+T1)*(T+T2);
	typ a[2]={-T1*T2/t,(T*T1+T*T2+2*T1*T2)/t};
	typ b[3]={0,0,kp*T*T/t};
	return Differenzengleichung<typ>(2,a,b);
}

/** 
 * PT2s-Glied
 * T     Abtastzeit in s
 * kp    Verstärkung
 * D     Dämpfung
 * wn    natürlichen Kreisfrequenz
 * G(s)= kp/(1 + 2*D*Tn*s + Tn^2*s^2)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PT2s(typ T, typ kp, typ D, typ wn) {
    typ t = 1/(1+wn*wn*T*T+2*D*wn*T);
	typ a[2]={-t,2*(1+D*wn*T)*t};
	typ b[3]={0,0,kp*wn*wn*T*T*t};
	return Differenzengleichung<typ>(2,a,b);
}

/** 
 * I-Glied
 * T     Abtastzeit in s
 * Ti    Integrationszeit
 * G(s)= 1/s/Ti
 */
template<typename typ>
Differenzengleichung<typ> DGL_I(typ T, typ Ti) {
    typ a[1]={1};
	typ b[2]={0,T/Ti};
	return Differenzengleichung<typ>(1,a,b);
}

/** PI-Glied
 * T     Abtastzeit in s
 * kp    Proportionalverstärkung
 * Tn    Nachstellzeit
 * G(s)= kp*(1+1/s/Tn)
 */
template<typename typ>
Differenzengleichung<typ> DGL_PI(typ T, typ kp, typ Tn) {
    typ a[1]={1};
	typ b[2]={-kp,kp*(1+T/Tn)};
	return Differenzengleichung<typ>(1,a,b);
}

/** 
 * PIDT1-Glied
 * T     Abtastzeit in s
 * kp    Proportionalverstärkung
 * Tn    Nachstellzeit I
 * Tv    Vorhaltezeit  D
 * T1    Verzögerungszeitkonstante
 * G(s)= kp*(1 + 1/s/Tn + sTv/(1+sT1))
 */
template<typename typ>
Differenzengleichung<typ> DGL_PIDT1(typ T, typ kp, typ Tn, typ Tv, typ T1) {
    typ a[2]={-T1/(T1+T),(2*T1+T)/(T1+T)};
	typ b[3]={kp*(T1+Tv)/(T1+T),-kp*(2*Tn*(T1+Tv)+T*(T1+Tn))/Tn/(T1+T),kp*(Tn*(T+T1+Tv)+T*(T1+T))/Tn/(T1+T)};
	return Differenzengleichung<typ>(2,a,b);
}

/**
 * Allpass-Glied
 * T     Abtastzeit in s
 * kp    Proportionalverstärkung
 * T1    Nullstellenzeitkonstante
 * T2    Polstellenzeitkonstante
 * G(s)= kp*(1-sT1)/(1+sT2)
 */
template<typename typ>
Differenzengleichung<typ> DGL_Allpass(typ T, typ kp, typ T1,typ T2) {
    typ a[1]={T2/(T+T2)};
	typ b[2]={kp*T1/(T+T2),kp*(T-T1)/(T+T2)};
	return Differenzengleichung<typ>(1,a,b);
}

#endif