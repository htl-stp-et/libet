/**
 * @file Long128.cpp
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Implementiert 128 Bit Integer Funktionen
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "./Long128.h"

/**
 * Maximaler Wert, welcher mit 128-Bit
 * gespeichert werden kann.
 */
Long128 LONG128INF  = Long128(0x7FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF);
/**
 * Minimaler Wert, welcher mit 128-Bit
 * gespeichert werden kann.
 */
Long128 LONG128NINF = Long128(0x8000000000000000, 0x0000000000000001);
/**
 * Wert, welcher bei ungültigen Rechenergebnissen zurückgegeben wird.
 * z.B.: 0/0
 */
Long128 LONG128NAN  = Long128(0x8000000000000000, 0x0000000000000000);

/**
 * Diese Funktion berechnet die Anzahl der verwendeten Bits
 * einer 128-Bit Zahl `a`.
 * 
 * @param  Long128 a ... Zahl, deren Bitlänge berechnet werden soll
 * @return int length ... Bitlänge von `a`
 */
int bitlength(Long128 a) {
	int length = 0;
	Long128 j = 1;
	for (int i = 0; j < a; i++) {
		j <<= 1;
		length = i;
	}
	return length+1;
}

/**
 * Diese Funktion erzeugt eine 128-Bit große Maske,
 * welche die komplette Long128 `a` umfasst.
 * 
 * @param  Long128 a ... Zahl welche die Basis der Maske wird
 * @return Long128 mask ... Die Maske von `a`
 */
Long128 bitlengthmask(Long128 a) {
	Long128 mask = 0;
	Long128 j = 1;
	for (int i = 0; j < a; i++) {
		j <<= 1;
		mask <<= 1;
		mask |= 1;
	}
	return mask;
}

/**
 * Der Default-Konstruktor.
 * Die 128-Bit-Zahl hat den Wert 0.
 */
Long128::Long128() {
	high = 0;
	low = 0;
}

/**
 * Konstruktor für kleine Zahlen (max. 64-Bit).
 * @param low ... 64-Bit-Ganzzahl
 */
Long128::Long128(int64_t low) {
	this->low = low;
	if (low < 0) high = -1;
	else high = 0;
}

/**
 * Konstruktor für große Zahlen (128-Bit).
 * @param low ... 64-Bit-Zahl für Bits 0->63
 * @param high ... 64-Bit-Zahl für Bits 64->127
 */
Long128::Long128(int64_t high, uint64_t low) {
	this->high = high;
	this->low = low;
}

/**
 * Addiert zu der Zahl einen Wert hinzu.
 * @param Long128 a ... Ein Summand der Addition
 */
Long128 Long128::operator+(const Long128& a) {
	Long128 ret;
	ret.high = high + a.high;
	ret.low = low + a.low;
	if (ret.low < low) {
		ret.high += 1;
	}
	if (high > 0 && a.high > 0 && ret.high < high) {
		return LONG128INF;
	} else if (high < 0 && a.high < 0 && ret.high > high) {
		return LONG128NINF;
	}
	return ret;
}

/**
 * Negiert die Zahl mithilfe des 2er Komplements.
 */
Long128 Long128::operator-() {
	/**
	 * Verwendet wird hierzu das 2er komplement.
	 */
	// Long128 ret;
	// ret.high = ~high;
	// ret.low = ~low;

	// if (low==0xFFFFFFFF) {
	// 	ret.high += 1;
	// }
	// ret.low += 1;
	// return ret;
	return ~*this +1;
}

/**
 * Subtrahiert von der Zahl einen Wert weg.
 * @param Long128 a ... Der Subtrahend der Subtraktion
 */
Long128 Long128::operator-(Long128 a) {
	return *this + (-a);
}

/**
 * Multipliziert die Zahl mit einem Wert.
 * @param Long128 a ... Der Multiplikator der Multiplikation
 */
Long128 Long128::operator*(Long128 a) {
	if (*this == 0 || a == 0) return 0;

	Long128 copyOfThis = *this;
	bool isNegative = false;
	if (copyOfThis < 0) {
		copyOfThis = -copyOfThis;
		isNegative = true;
	}
	if (a < 0) {
		a = -a;
		isNegative = !isNegative;
	}

	Long128 result;
	if ((copyOfThis & -copyOfThis) == copyOfThis) {
		int n = bitlength(copyOfThis);
		result = a << n;
		if (isNegative) return -result;
		else return result;
	} else if ((a & -a) == a) {
		int n = bitlength(a);
		result = copyOfThis << n;
		if (isNegative) return -result;
		else return result;
	}

	result = ((exactMul(copyOfThis.high, a.low) + exactMul(copyOfThis.low, a.high))<<64) + exactMul(copyOfThis.low, a.low);
	if (isNegative) return -result;
	else return result;
}

/**
 * Dividiert die Zahl durch einen Wert.
 * @param Long127 divisor ... Der Divisor der Division
 */
Long128 Long128::operator/(Long128 divisor) {
	Long128 result = 0;
	Long128 dividend = *this;

	bool isNegative = false;

	if (dividend < 0) {
		dividend = -dividend;
		isNegative = true;
	}
	if (divisor < 0) {
		divisor = -divisor;
		isNegative = !isNegative;
	}


	/**
	 * Für eine schnellere Berechnung wird geprüft,
	 * ob der Divisor 2^n ist. Wenn er das ist, kann
	 * einfach Zahl>>n gerechnet werden.
	 */
	// Ob eine Zahl 2^n ist, kann man über diese Zeile feststellen:
	if ((divisor & -divisor) == divisor) {
		// Wenn eine Zahl mit ihrem 2er Komplement verundet wird,
		// und das Ergebnis ist wieder die Zahl, dann ist die Zahl 2^n.

		if (divisor == 0) {
			if (dividend == 0) return LONG128NAN;
			if (!isNegative) return LONG128INF;
			if (isNegative) return LONG128NINF;
		}
		
		int n = bitlength(divisor);
		result =  dividend >> n;
		if (isNegative) return -result;
		else return result;
	} else if (divisor == LONG128INF || divisor == LONG128NINF || dividend < divisor) {
		return 0;
	}
	
	// divisor formatieren
	Long128 divisorMask = bitlengthmask(divisor);
	int divisorLength = bitlength(divisor);
	int dividendLength = bitlength(dividend);

	// Quasi händische division als Algorithmus
	for (int i = 0; i < dividendLength - divisorLength +1; i++) {
		result <<= 1;
		Long128 smallDividend = (dividend & (divisorMask << (dividendLength-i-divisorLength+1)))>>(dividendLength-i-divisorLength);
		
		if (smallDividend > divisor) {
            dividend -= divisor<<(dividendLength-divisorLength-i);
            result |= 1;
        } else {
			divisorMask |= ((Long128)1<<(i + divisorLength));
		}
	}

	if (isNegative) {
		return -result;
	} else {
		return result;
	}
}

/**
 * Schiebt die Zahl um einen Wert nach links.
 * @param int a ... Anzahl der Linksschübe
 */
Long128 Long128::operator<<(int a) {
	Long128 ret;
	if (a < 64) {
		ret.high = (high << a) | (low >> (64-a));
		ret.low = (low << a);
	} else if (a < 128) {
		ret.high = low<<(a-64);
		ret.low = 0;
	}
	return ret;
}

/**
 * Schiebt die Zahl um einen Wert nach rechts.
 * @param int a ... Anzahl der Rechtsschübe
 */
Long128 Long128::operator>>(int a) {
	Long128 ret;
	if (a < 64) {
		ret.high = high >> a;
		ret.low = (low >> a) | (high << (64-a));
	} else if (a < 128) {
		ret.high = 0;
		ret.low = high >> (a-64);
	}
	return ret;
}

/**
 * Verodert die Zahl mit einem Wert.
 * 
 * Wird bei Bitmustern bis 64-Bit verwendet, da sich
 * so eine Rechenoperation gespart wird.
 * 
 * @param uint64_t a ... Zahl mit welcher geodert wird
 */
Long128 Long128::operator|(uint64_t a) {
		return Long128(high, low | a);
}

/**
 * Verodert die Zahl mit einem Wert.
 * 
 * @param Long128 a ... Zahl mit welcher geodert wird
 */
Long128 Long128::operator|(Long128 a) {
		return Long128(high | a.high, low | a.low);
}

/**
 * Maskiert die Zahl mit einer Maske.
 * 
 * Wird bei Bitmustern bis 64-Bit verwendet, da sich
 * so eine Rechenoperation gespart wird.
 * 
 * @param uint64_t a ... Die Maske der Operation
 */
Long128 Long128::operator&(uint64_t a) {
	return Long128(0, low & a);
}

/**
 * Maskiert die Zahl mit einer Maske.
 * 
 * @param Long128 a ... Die Maske der Operation
 */
Long128 Long128::operator&(Long128 a) {
	return Long128(high & a.high, low & a.low);
}

/**
 * Führt die binäre Negation durch.
 */
Long128 Long128::operator~() {
    return Long128(~this->high, ~this->low);
}

/**
 * Führt eine XOR Operation durch.
 *
 * Wird bei Bitmustern bis 64-Bit verwendet, da sich
 * so eine Rechenoperation gespart wird.
 * 
 * @param Long128 a ... Die Maske der Operation
 */
Long128 Long128::operator^(uint64_t a) {
	return Long128(high, low ^ a);
}

/**
 * Führt eine XOR Operation durch.
 * 
 * @param Long128 a ... Die Maske der Operation
 */
Long128 Long128::operator^(Long128 a) {
	return Long128(high ^ a.high, low ^ a.low);
}

/**
 * Es wird verglichen, ob die Zahl kleiner ist als ein Wert.
 * 
 * @param Long128 a ... Vergleichswert
 */
bool Long128::operator<(Long128 a) {
	if (high < a.high) {
		return true;
	} else if (high == a.high && low < a.low) {
		return true;
	} else {
		return false;
	}
}

/**
 * Es wird verglichen, ob die Zahl größer ist als ein Wert.
 * 
 * @param Long128 a ... Vergleichswert
 */
bool Long128::operator>(Long128 a) {
	if (high > a.high) return true;
	if (high < a.high) return false;
	if (low > a.low) return true;
	return false;
}

/**
 * Es wird die boolesche nicht Operation durchgeführt.
 */
bool Long128::operator!() {
	if (*this == 0) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * Es wird verglichen, ob die Zahl gleich einem Wert ist.
 * 
 * @param Long128 a ... Vergleichswert
 */
bool Long128::operator==(Long128 a) {
	if (high == a.high && low == a.low) return true;
	return false;
}

/**
 * Es wird verglichen, ob die Zahl kleiner oder gleich einem Wert ist.
 * 
 * @param Long128 a ... Vergleichswert
 */
bool Long128::operator<=(Long128 a) {
	if (*this == a || *this < a) return true;
	return false;
}

/**
 * Es wird verglichen, ob die Zahl größer oder gleich einem Wert ist.
 * 
 * @param Long128 a ... Vergleichswert
 */
bool Long128::operator>=(Long128 a) {
	if (*this == a || *this > a) return true;
	return false;
}

/**
 * Es wird verglichen, ob die Zahl ungleich einem Wert ist.
 * 
 * @param Long128 a ... Vergleichswert
 */
bool Long128::operator!=(Long128 a) {
	if (*this == a) {
		return false;
	} else {
		return true;
	}
}

/**
 * Addiert zu der Zahl einen Wert hinzu und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Summand
 */
Long128& Long128::operator+=(Long128 a) {
	*this = *this + a;
	return *this;
}

/**
 * Subtrahiert von der Zahl einen Wert weg und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Subtrahend
 */
Long128& Long128::operator-=(Long128 a) {
    *this = *this - a;
    return *this;
}

/**
 * Multipliziert die Zahl mit einem Wert und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Multiplikator
 */
Long128& Long128::operator*=(Long128 a) {
	*this = *this * a;
	return *this;
}

/**
 * Dividiert die Zahl durch einen Wert und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Divisor
 */
Long128& Long128::operator/=(Long128 divisor) {
	*this = *this / divisor;
	return *this;
}

/**
 * Verodert die Zahl mit einer Maske und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Maske
 */
Long128& Long128::operator|=(Long128 a) {
    *this = *this | a;
    return *this;
}

/**
 * Maskiert die Zahl mit einer Maske und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Maske
 */
Long128& Long128::operator&=(Long128 a) {
    *this = *this & a;
    return *this;
}

/**
 * Schiebt die Zahl um einen Wert nach links und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Anzahl der Verschiebungen
 */
Long128& Long128::operator<<=(int a) {
	*this = *this << a;
	return *this;
}


/**
 * Schiebt die Zahl um einen Wert nach rechts und
 * weist der Zahl das Ergebnis zu.
 * 
 * @param Long128 a ... Anzahl der Verschiebungen
 */
Long128& Long128::operator>>=(int a) {
	*this = *this >> a;
	return *this;
}

/**
 * Weist der Zahl einen neuen Wert zu.
 * 
 * @param Long128 a ... Neuer Wert der Zahl
 */
Long128& Long128::operator=(const Long128& a) {
	high = a.high;
	low = a.low;
	return *this;
}

/**
 * Es werden 2 Ganzzahlen mit 64-Bit miteinander multipliziert.
 * Das Ergebnis ist eine 128-Bit Zahl.
 * Hierdurch wird die Genauigkeit des Ergebnisses erhöht.
 * 
 * @param int64_t a ... Faktor 1
 * @param int64_t b ... Faktor 2
 * 
 * @return Long128 ret ... Produkt der Multiplikation mit 128-Bit Genauigkeit
 */
Long128 exactMul(int64_t a, int64_t b) {
	if (a == 0 || b == 0) {
		return 0;
	}

	Long128 ret;
	bool isNegative = false;

	if (a < 0) {
		a = -a;
		isNegative = true;
	}
	if (b < 0) {
		b = -b;
		isNegative = !isNegative;
	}

	/**
	 * Für eine schnellere Berechnung wird geprüft,
	 * ob einer der Teile 2^n ist. Wenn er das ist, kann
	 * einfach anderer Teil<<n gerechnet werden.
	 */
	// Ob eine Zahl 2^n ist, kann man über diese Zeile feststellen:
	if ((a & -a) == a) {
		// Wenn eine Zahl mit ihrem 2er Komplement verundet wird,
		// und das Ergebnis ist wieder die Zahl, dann ist die Zahl 2^n.
		
		int n = bitlength(a);
		ret =  (Long128)b << n;
		if (isNegative) return -ret;
		else return ret;
	} else if ((b & -b) == b) {
		int n = bitlength(b);
		ret =  (Long128)a << n;
		if (isNegative) return -ret;
		else return ret;
	}

	uint64_t ah, al, bh, bl;

	ah = a>>32;
	al = a&0xFFFFFFFF;
	bh = b>>32;
	bl = b&0xFFFFFFFF;

	ret = (Long128(ah * bh)<<64) +
	 ((Long128(ah*bl) + Long128(al*bh)) << 32)+
	 Long128(al*bl);

	if (isNegative) return -ret;
	return ret;
}
