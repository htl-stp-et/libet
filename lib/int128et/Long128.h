/**
 * Long128.h
 * 
 * 128 bit Integer
 * 
 * Created on: 18. Dez. 2015 (Author: damboeck)
 * 
 * Changelog:
 * 16.10.2018, Lukas Heumesser
 * Platformunabhängige Typenlängen
 * int -> int32_t
 * long -> int32_t
 * long long -> int64_t
 * 
 * 5.10.2018, Andreas Kohler
 * Verbesserung der Übersicht und Objektorientierte Version
 * 
 */

#ifndef LONG128_H_
#define LONG128_H_

#include <inttypes.h>

class Long128 {
private:
	
public:
	volatile int64_t high;
	volatile uint64_t low;

	// Konstruktoren
	Long128();
	Long128(int64_t low);
	Long128(int64_t high, uint64_t low);

	explicit operator int64_t() { return low; }
	explicit operator uint64_t() { return low; }

	// Mathematische Operatoren
	Long128 operator+(const Long128& a);
	Long128 operator-();
	Long128 operator-(Long128 a);
	Long128 operator*(Long128 a);
	Long128 operator/(Long128 divisor);

	// Binäre Operatoren
	Long128 operator<<(int a); 
	Long128 operator>>(int a); 
	Long128 operator|(uint64_t a); // Performance improvement
	Long128 operator|(Long128 a); 
	Long128 operator&(uint64_t a); // Performance improvement
	Long128 operator&(Long128 a); 
	Long128 operator^(uint64_t a); // Performance improvement
	Long128 operator^(Long128 a);
    Long128 operator~();

	// Bool'sche Operatoren
	bool operator<(Long128 a);
	bool operator>(Long128 a);
	bool operator!();
	bool operator==(Long128 a);
	bool operator<=(Long128 a);
	bool operator>=(Long128 a);
	bool operator!=(Long128 a);

	// Zuweisungs-Operatoren
	Long128& operator+=(Long128 a);
	Long128& operator-=(Long128 a);
	Long128& operator*=(Long128 a);
	Long128& operator/=(Long128 divisor);
    Long128& operator|=(Long128 a);
    Long128& operator&=(Long128 a);
	Long128& operator<<=(int a);
	Long128& operator>>=(int a);
	Long128& operator=(const Long128& a);
};

typedef Long128 int128_t;

extern int128_t LONG128INF;
extern int128_t LONG128NINF;
extern int128_t LONG128NAN;

int128_t exactMul(int64_t a, int64_t b);

#endif
