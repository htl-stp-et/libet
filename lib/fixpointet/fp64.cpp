/**
 * @file fp64.cpp
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Implementiert von der Fixpoint-Library benötigte Variablen
 * @version 0.1
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "./fp64.h"

template<> int Fixpoint<int64_t, int128_t, 24>::KOMMA_ = 24;
template<> int64_t Fixpoint<int64_t, int128_t, 24>::INF_ = 0x7FFFFFFFFFFFFFFF;
template<> int64_t Fixpoint<int64_t, int128_t, 24>::NINF_ = 0x8000000000000001;
template<> int64_t Fixpoint<int64_t, int128_t, 24>::NAN_ = 0x8000000000000001;
template<> int64_t Fixpoint<int64_t, int128_t, 24>::MAX_ = (int64_t(1) << (64 - 24 - 1))-1;
template<> int64_t Fixpoint<int64_t, int128_t, 24>::MIN_ = -Fixpoint<int64_t, int128_t, 24>::MAX_;