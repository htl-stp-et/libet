/**
 * @file fixpoints.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Dient zum Einbinden von 32 und 64 Bit Fixpointdefinitionen.
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef FIXPOINTS_H
#define FIXPOINTS_H

#include "./fp32.h"
#include "./fp64.h"

#endif