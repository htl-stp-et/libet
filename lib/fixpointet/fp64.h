/**
 * @file fp64.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Dient zum Einbinden von 64 Bit Fixpointdefinitionen.
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef FIXPOINT64_H
#define FIXPOINT64_H

#include "./fp.h"
#include <inttypes.h>
#include "./Long128.h"

using fixpoint64_t = Fixpoint<int64_t, int128_t, 24>;

#endif