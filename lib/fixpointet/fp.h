/**
 * @file fp.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Diese Datei stellt das Grundgerüst für Fixpoint-Variablen bereit.
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef FIXPOINT_H_
#define FIXPOINT_H_

/**
 * @brief Eine Klasse welche Fixkommazahlen Berechnungen durchführen kann.
 * 
 * @tparam base_t Basistyp der Fixpoint Klasse, für eine 32-Bit Fixpoint: int32_t
 * @tparam big_t  Nächstgrößerer Typ von Basistyp, für eine 32-Bit Fixpoint: int64_t
 * @tparam KOMMA  Nachkommastellen der Fixpoint
 */
template<typename base_t, typename big_t, int KOMMA>
class Fixpoint {
private:
	base_t value_;
public:
	static base_t INF_;
	static base_t NINF_;
	static base_t NAN_;
	static base_t MAX_;
	static base_t MIN_;
	static int KOMMA_;

	Fixpoint() : value_(0) {}
	/**
	 * @brief Erstellt ein neues Fixpoint-Objekt
	 * 
	 * @param a 			Der neue Wert der Fixpoint
	 * @param setDirectly 	Wenn die neue Variable im Fixpoint Format der Klasse ist soll dieser Wert auf `true` gesetzt werden.
	 */
	Fixpoint(base_t a, bool setDirectly) : value_(a) {
		if (setDirectly) value_ = a;
		else value_ = a << KOMMA;
	}
	/**
	 * @brief Erstellt aus einer Ganzzahl ein Fixpoint-Objekt
	 * 
	 * @param a 	Ganzzahl, welche in eine Fixpoint Zahl umgewandelt werden soll.
	 */
	Fixpoint(int a) :value_(base_t(a)<<KOMMA) {}
	/**
	 * @brief Erstellt aus einer Double ein Fixpoint-Objekt
	 * 
	 * @param a 	Double, welche in eine Fixpoint Zahl umgewandelt werden soll.
	 */
	Fixpoint(double a) : value_(a * (base_t(1)<<KOMMA)) {}

	/**
	 * @brief Wandelt die Fixpoint Zahl in eine Gleitkommazahl um
	 * 
	 * @return double Gleitkommadarstellung der Fixpoint
	 */
	double toDouble() { return double(value_) / (base_t(1)<<KOMMA); }
	/**
	 * @brief Liefert den Variablenwert direkt zurück
	 * 
	 * @return base_t Zahl in Fixpoint Darstellung
	 */
	base_t getValue() { return value_; }

	Fixpoint operator+(Fixpoint a) { return Fixpoint(value_ + a.value_, true); }
	Fixpoint operator-() { return Fixpoint(-this->value_, true); }
	Fixpoint operator-(Fixpoint a) { return *this + (-a); }

	Fixpoint operator*(Fixpoint a) {
		if (this->value_ == 0 || a.value_ == 0) return 0;

		big_t ergebnis = ((big_t)this->value_ * (big_t)a.value_) >> KOMMA;
		Fixpoint ret;
		ret.value_ = (base_t)ergebnis;

		return ret;
	}

	Fixpoint operator/(Fixpoint divisor) {
		// if (!_isInit) _init();
		
		if (divisor == 0) {
			if (value_ > 0) return Fixpoint(INF_, true);
			else if (value_ < 0) return Fixpoint(NINF_, true);
			else return Fixpoint(NAN_, true);
		}
		return Fixpoint(base_t(((big_t)this->value_ << KOMMA) / divisor.value_), true);
	}

	bool operator==(Fixpoint a) { return this->value_ == a.value_; }
	bool operator<(Fixpoint a) { return this->value_ < a.value_; }
	bool operator<=(Fixpoint a) { return (this->value_ < a.value_ || this->value_ == a.value_); }
	bool operator>(Fixpoint a) { return this->value_ > a.value_; }
	bool operator>=(Fixpoint a) { return (this->value_ > a.value_ || this->value_ == a.value_); }

	/**
	 * @briefPotenziert die Zahl mit a. (zahl^a)
	 * 
	 * @return ... Ergebnis der Rechnung.
	 */
	Fixpoint pow(int a) {
		if (a < 0)  return Fixpoint(1 / this->pow(-a).value_, true);
		if (a == 0) return 1;
		if (a == 1) return *this;

		Fixpoint ret = *this;
		for (int i = 1; i < a; i++)
			ret = ret * *this;
		return ret;
	}

	/**
	 * @brief Invertiert die Zahl.
	 * *WICHTIG!*: 
	 * Direkte Division ist um ein vielfaches schneller als mit der invertierten Zahl zu multiplizieren!
	 * Daher sollte man wenn möglich `/` benutzen!
	 * 
	 * @return Invertierte Zahl
	 */
	Fixpoint invert() {
		// if (!_isInit) _init();
        if (value_ >= INF_ || value_ <= NINF_) return 0;
        if (value_ == 0) return Fixpoint(INF_, true);

        return Fixpoint(base_t(((big_t(1)<<KOMMA)<<KOMMA) / big_t(value_)), true);
    }

	/**
	 * @brief Zieht die Wurzel aus der Zahl.
	 * 
	 * @return Die Wurzel der Zahl
	 */
	Fixpoint sqrt() {
		// if (!_isInit) _init();
        if (value_ < 0) return Fixpoint(NAN_, true);

        base_t v = value_ << KOMMA;

        base_t c = 0;
        base_t bit;
        int s = sizeof(base_t)*8/2-1;

        do {
            s--;
            bit = (1<<s);
            c |= bit;
            if ((c*c) > v) c &=~ bit;
        } while(s > 0);
        return Fixpoint(c, true);
    }

	Fixpoint& operator+=(Fixpoint a) { return *this = *this + a; }
	Fixpoint& operator-=(Fixpoint a) { return *this = *this - a; }
	Fixpoint& operator*=(Fixpoint a) { return *this = *this * a; }
	Fixpoint& operator/=(Fixpoint a) { return *this = *this / a; }


	friend class Winkel;

	template<typename fixpoint_t>
	friend class Complex;

	template<typename type_t>
	friend class Norm;
};

#endif