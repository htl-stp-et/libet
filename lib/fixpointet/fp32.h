/**
 * @file fp32.h
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Dient zum Einbinden von 32 Bit Fixpointdefinitionen.
 * @version 1.0
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef FIXPOINT32_H
#define FIXPOINT32_H

#include "./fp.h"
#include <inttypes.h>

using fixpoint32_t = Fixpoint<int32_t, int64_t, 12>;

#endif