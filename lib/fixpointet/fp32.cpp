/**
 * @file fp32.cpp
 * @author Andreas Kohler (andreas.kohler@htlstp.at)
 * @brief Implementiert von der Fixpoint-Library benötigte Variablen
 * @version 0.1
 * @date 2020-02-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "./fp32.h"

template<> int Fixpoint<int32_t, int64_t, 12>::KOMMA_ = 12;
template<> int32_t Fixpoint<int32_t, int64_t, 12>::INF_ = 0x7FFFFFFF;
template<> int32_t Fixpoint<int32_t, int64_t, 12>::NINF_ = 0x80000001;
template<> int32_t Fixpoint<int32_t, int64_t, 12>::NAN_ = 0x80000000;
template<> int32_t Fixpoint<int32_t, int64_t, 12>::MAX_ = (int32_t(1) << (32 - 12 - 1))-1;
template<> int32_t Fixpoint<int32_t, int64_t, 12>::MIN_ = -Fixpoint<int32_t, int64_t, 12>::MAX_;