#pragma once

#include <Arduino.h>


class EasySpi{
    public:
        EasySpi(uint8_t datamode,uint8_t dataorder,uint8_t clkpresacler);
        void send(uint8_t *sendArray, uint16_t length, void runFunction(uint8_t *returnedArray, uint16_t length));
        void sendPoll(uint8_t *sendArray, uint16_t length);
};