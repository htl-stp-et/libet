#include <EasySpi.hpp>

#define BUFFERSIZE 16

static int count;
static int arrayLength;
static uint8_t bufferArray[BUFFERSIZE];

static void (*funcPtr)(uint8_t *array, uint16_t length);

#ifdef __AVR_ATmega2560__
    /**
     * datamode:    0: CPOL=0, CPHA=0
     *              1: CPOL=0, CPHA=1
     *              2: CPOL=1, CPHA=0
     *              3: CPOL=1, CPHA=1
     * 
     * clkprescaler:    0:  4
     *                  1:  16
     *                  2:  64 
     *                  3:  128
     *                  4:  2
     *                  5:  8
     *                  6:  32
     *                  7:  64
     */
    EasySpi::EasySpi(uint8_t datamode,uint8_t dataorder,uint8_t clkprescaler){

        DDRB |= (1<<PB0) | (1<<PB1) | (1<<PB2);
        DDRB &= ~(1<<PB3);

        SPCR = (1 << SPE) | (1 << MSTR) | (datamode << CPHA) | (dataorder << DORD) | ((clkprescaler & 3) << SPR0);
        SPSR = ((clkprescaler & 4) << SPI2X);
    }

    void EasySpi::sendPoll(uint8_t *sendArray, uint16_t length){        
        SPCR &= ~(1 << SPIE);

        count = 0;

        for(uint8_t i=0; i<length; i++){
            SPDR = sendArray[i];

            while(!(SPSR & (1 << SPIF)));

            sendArray[i] = SPDR;
        }
    }

    void EasySpi::send(uint8_t *sendArray, uint16_t length, void runFunction(uint8_t *returnedArray, uint16_t length)){
        count = 0;

        arrayLength = length;

        funcPtr = runFunction;

        for(uint8_t i=0; i < length; i++){
            bufferArray[i] = sendArray[i];
        }

        SPCR |= (1 << SPIE);

        SPDR = bufferArray[0];
    }

    ISR(SPI_STC_vect){
        if(count < arrayLength){
            bufferArray[count] = SPDR;
            count++;
            if(!(count == arrayLength))
                SPDR = bufferArray[count];
        } 

        if(count == arrayLength){
            SPCR &= ~(1 << SPIE);

            funcPtr(bufferArray, arrayLength);
        }   
    }
#endif

#if defined (__arm__) && defined (__SAM3X8E__)
    #include <chip.h>
    EasySpi::EasySpi(uint8_t datamode,uint8_t dataorder,uint8_t clkprescaler){
        PIO_SetPeripheral(PIOA, PIO_PERIPH_A, PIO_PA25A_SPI0_MISO | PIO_PA26A_SPI0_MOSI | PIO_PA27A_SPI0_SPCK | PIO_PA29A_SPI0_NPCS1);

        pmc_enable_periph_clk(ID_SPI0) ;

        SPI0->SPI_CR = SPI_CR_SWRST;

        SPI0->SPI_MR = SPI_MR_MSTR | SPI_MR_PS;

        SPI0->SPI_CSR[1] = SPI_CSR_SCBR(clkprescaler) | (datamode << SPI_CSR_CPOL);

        NVIC_EnableIRQ(SPI0_IRQn);        

        SPI0->SPI_CR = SPI_CR_SPIEN;
    }

    void EasySpi::send(uint8_t *sendArray, uint16_t length, void runFunction(uint8_t *returnedArray, uint16_t length)){
        count = 0;

        arrayLength = length;

        funcPtr = runFunction;

        for(uint8_t i=0; i < length; i++){
            bufferArray[i] = sendArray[i];
        }        

        SPI0->SPI_IER = SPI_IER_RDRF;

        SPI0->SPI_TDR = SPI_TDR_PCS(1) | SPI_TDR_TD(bufferArray[0]);
    }

    void SPI0_Handler(){
        if(count < arrayLength){
            bufferArray[count] = (SPI0->SPI_RDR & 0xFF);
            count++;
            if(!(count == arrayLength))
                SPI0->SPI_TDR = SPI_TDR_PCS(1) | SPI_TDR_TD(bufferArray[count]);
        } 
        
        if(count == arrayLength){
            SPI0->SPI_IDR = SPI_IDR_RDRF;

            funcPtr(bufferArray, arrayLength);
        } 
    }
#endif

